package djf.ui.dialogs;

import djf.AppPropertyType;
import static djf.AppPropertyType.*;
import djf.AppTemplate;
import static djf.AppTemplate.PATH_WORK;
import djf.modules.AppRecentWorkModule;
import djf.modules.AppLanguageModule;
import djf.modules.AppLanguageModule.LanguageException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 * @author McKillaGorilla
 */
public class AppDialogsFacade {
    public static File imageFile;
    public static void showAboutDialog(AppTemplate app) {
        AppWebDialog dialog = new AppWebDialog(app);
        try {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String webPath = props.getProperty(APP_PATH_WEB);
            String filePath = webPath + props.getProperty(APP_ABOUT_PAGE);
            dialog.showWebDialog(filePath);
        }
        catch(MalformedURLException murle) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), ABOUT_DIALOG_ERROR_TITLE, ABOUT_DIALOG_ERROR_CONTENT);
        }
    }
    public static File showExportDialog(Stage window, AppPropertyType exportTitleProp)  {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fc.setTitle(props.getProperty(exportTitleProp));
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXPORT_EXT), props.getProperty(WORK_FILE_EXPORT_EXT_DESC))
        );
        File selectedFile = fc.showSaveDialog(window);
        return selectedFile;
    }
    public static File showSaveDialog(Stage window, AppPropertyType saveTitleProp) {
        // PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fc.setTitle(props.getProperty(saveTitleProp));
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT))
        );
        File selectedFile = fc.showSaveDialog(window);
        return selectedFile;
    }
    public static void showHelpDialog(AppTemplate app) {
        AppWebDialog dialog = new AppWebDialog(app);
        try {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String webPath = props.getProperty(APP_PATH_WEB);
            String filePath = webPath + props.getProperty(APP_HELP_PAGE);
            dialog.showWebDialog(filePath);
        }
        catch(MalformedURLException murle) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), HELP_DIALOG_ERROR_TITLE, HELP_DIALOG_ERROR_CONTENT);
        }
    }
    public static void showLanguageDialog(AppLanguageModule languageSettings) throws LanguageException {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String currentLanguage = languageSettings.getCurrentLanguage();
        ArrayList<String> languages = languageSettings.getLanguages();
        ChoiceDialog<String> languageDialog = new ChoiceDialog<>(currentLanguage, languages);
        String title = props.getProperty(LANGUAGE_DIALOG_TITLE);
        String headerText = props.getProperty(LANGUAGE_DIALOG_HEADER_TEXT);
        String contentText = props.getProperty(LANGUAGE_DIALOG_CONTENT_TEXT);
        languageDialog.setTitle(title);
        languageDialog.setHeaderText(headerText);
        languageDialog.setContentText(contentText);
        languageDialog.showAndWait();
        String selectedLanguage = languageDialog.getSelectedItem();
        if (selectedLanguage != null) {
            languageSettings.setCurrentLanguage(selectedLanguage);
        }
    }
    public static void showMessageDialog(Stage parent, Object titleProperty, Object contentTextProperty) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(titleProperty);
        String contentText = props.getProperty(contentTextProperty);
        Alert messageDialog = new Alert(Alert.AlertType.INFORMATION);
        messageDialog.initOwner(parent);
        messageDialog.initModality(Modality.APPLICATION_MODAL);
        messageDialog.setTitle(title);
        messageDialog.setHeaderText("");
        Label label = new Label(contentText);
        label.setWrapText(true);
        messageDialog.getDialogPane().setContent(label);
        messageDialog.showAndWait();
    }
    public static File showOpenDialog(Stage window, AppPropertyType openTitleProp) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(window);        
        return selectedFile;
    }

    public static void showStackTraceDialog(Stage parent, Exception exception,
            Object appErrorTitleProperty,
            Object appErrorContentProperty) {
        // FIRST MAKE THE DIALOG
        Alert stackTraceDialog = new Alert(Alert.AlertType.ERROR);
        stackTraceDialog.initOwner(parent);
        stackTraceDialog.initModality(Modality.APPLICATION_MODAL);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        stackTraceDialog.setTitle(props.getProperty(appErrorTitleProperty));
        stackTraceDialog.setContentText(props.getProperty(appErrorContentProperty));

        // GET THE TEXT FOR THE STACK TRACE
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String exceptionText = sw.toString();

        // AND PUT THE STACK TRACE IN A TEXT ARA
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        stackTraceDialog.getDialogPane().setExpandableContent(textArea);

        // OPEN THE DIALOG
        stackTraceDialog.showAndWait();
    }
    public static String showTextInputDialog(Stage parent, Object titleProperty, Object contentProperty) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(titleProperty);
        String contentText = props.getProperty(contentProperty);
        TextInputDialog textDialog = new TextInputDialog();
        textDialog.initOwner(parent);
        textDialog.initModality(Modality.APPLICATION_MODAL);
        textDialog.setTitle(title);
        textDialog.setContentText(contentText);
        Optional<String> result = textDialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        }
        return "";
    }
    public static String showWelcomeDialog(AppTemplate app) {

        // FIRST LOAD ALL THE RECENT WORK
        AppRecentWorkModule recentWork = app.getRecentWorkModule();
        recentWork.loadRecentWorkList();

        // OPEN THE DIALOG
        AppWelcomeDialog wd = new AppWelcomeDialog(app);
        wd.showAndWait();
        
        // AND RETURN THE USER SELECTION
        return wd.selectedWorkName;
    }
    public static ButtonType showYesNoCancelDialog(Stage parent, Object titleProperty, Object contentTextProperty) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(titleProperty);
        String contentText = props.getProperty(contentTextProperty);
        Alert confirmationDialog = new Alert(Alert.AlertType.CONFIRMATION);
        confirmationDialog.initOwner(parent);
        confirmationDialog.initModality(Modality.APPLICATION_MODAL);
        confirmationDialog.getButtonTypes().clear();
        confirmationDialog.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        confirmationDialog.setTitle(title);
        confirmationDialog.setContentText(contentText);
        Optional<ButtonType> result = confirmationDialog.showAndWait();
        return result.get();
    }

    public static Optional<Pair<String, String>> showChangeDimensionDialog(String title, String header,double currentWidth,double currentHeight) {
                // Create the custom dialog.
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.setHeaderText(header);

        // Set the icon (must be included in the project).


        // Set the button types.
        ButtonType confirmButtonType = new ButtonType("Confirm", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField widthField = new TextField();
        widthField.setPromptText(""+currentWidth);
        TextField heightField = new TextField();
        heightField.setPromptText(""+currentHeight);

        grid.add(new Label("Width:"), 0, 0);
        grid.add(widthField, 1, 0);
        grid.add(new Label("Height:"), 0, 1);
        grid.add(heightField, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node confirmButton = dialog.getDialogPane().lookupButton(confirmButtonType);
        confirmButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        widthField.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmButton.setDisable(newValue.trim().isEmpty());
        });
        heightField.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> widthField.requestFocus());
        Platform.runLater(() -> heightField.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == confirmButtonType) {
                return new Pair<>(widthField.getText(), heightField.getText());
            }
            return null;
        });

        return dialog.showAndWait();
        
    }
    
    public static Image showChooseImageDialog() {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("./images/"));
            FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
            FileChooser.ExtensionFilter extFilterGIF = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
            FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
            FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
            fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterGIF, extFilterJPG, extFilterPNG);
            fileChooser.setSelectedExtensionFilter(extFilterPNG);

            // OPEN THE DIALOG
            File file = fileChooser.showOpenDialog(null);
            if(file==null){return null;}
            imageFile = file;
            try {
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                return image;
            } catch (IOException ex) {
                return null;
        }
    }

}