package glgl.files;

import static djf.AppPropertyType.APP_EXPORT_PAGE;
import static djf.AppPropertyType.APP_PATH_EXPORT;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import glgl.data.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.web.WebEngine;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.swing.text.html.HTML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javafx.scene.Node;

import org.xml.sax.SAXException;
import properties_manager.PropertiesManager;
import static glgl.goLogoloPropertyType.*;
import glgl.data.goLogoloData;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;


import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javax.imageio.ImageIO;


/**
 *
 * @author McKillaGorilla
 */
public class goLogoloFiles implements AppFileComponent {
    // FOR JSON SAVING AND LOADING
    
    static final String JSON_SHAPES = "shapes";
        
    static final String JSON_ORDER = "order";
    static final String JSON_NAME = "name";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_ARCWIDTH = "arcWidth";
    static final String JSON_ARCHEIGHT = "arcHeight";
    static final String JSON_STROKEWIDTH = "strokeWidth";
    static final String JSON_STROKE = "strokeColor";    
    static final String JSON_FILL_COLOR = "fill_color";
    
    static final String JSON_FILEPATH="filePath";

    static final String JSON_CENTERX = "centerX";
    static final String JSON_CENTERY = "centerY";
    static final String JSON_RADIUS = "radius";
    
    static final String JSON_TEXT= "text";
    
    
    static final String JSON_FONT= "font";    
    static final String JSON_FONTNAME= "fontname";
    static final String JSON_FONTFAMILY= "fontfamily";
    static final String JSON_FONTSTYLE= "fontstyle";
    static final String JSON_FONTSIZE= "fontsize";

    static final String JSON_POINT1X = "point1_x";
    static final String JSON_POINT1Y = "point1_y";
    static final String JSON_POINT2X = "point2_x";
    static final String JSON_POINT2Y = "point2_y";
    static final String JSON_POINT3X = "point3_x";
    static final String JSON_POINT3Y = "point3_y";
    
    static final String JSON_FOCUSANGLE = "focusAngle:";
    static final String JSON_FOCUSDISTANCE = "focusDistance:";
    static final String JSON_CGCENTERX="cgcenterX:";
    static final String JSON_CGCENTERY="cgcenterY:";
    static final String JSON_CGRADIUS = "cgradius:";
    static final String JSON_CYCLEMETHOD = "cycleMethod:";
    static final String JSON_STOP0COLOR = "stop0_color:";
    static final String JSON_STOP1COLOR = "stop1_color:";    
    
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";


    
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	goLogoloData glglData = (goLogoloData)data;
	
	// FIRST THE LIST NAME AND OWNER
//	String name = toDoData.getName();
//        String owner = toDoData.getOwner();
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> shapes = glglData.getItems();
	for (Node node : shapes) {
//	    Shape shape = (Shape)node;
//	    Draggable draggableShape = ((Draggable)shape);
            if(node instanceof DraggableText){
                Shape shape = (Shape)node;
                Draggable draggableShape = ((Draggable)shape);
                String order = draggableShape.getOrder();
                String name  = draggableShape.getName();
                String type  = draggableShape.getType();
                double x = draggableShape.getX();
                double y = draggableShape.getY();
                double width = draggableShape.getWidth();
                double height = draggableShape.getHeight();
                String text = ((DraggableText)draggableShape).getText();
                //String font = ((DraggableText)draggableShape).getFont().toString();
                JsonObject fontInfoJson  = makeJsonFontObject(((DraggableText)draggableShape).getFont());
                JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());

                //JsonObject outlineColorJson = makeJsonColorObject((Color)shape.getStroke());
                //double outlineThickness = shape.getStrokeWidth();

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, order)
                        .add(JSON_NAME, name)
                        .add(JSON_TYPE, type)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_TEXT, text)
                        .add(JSON_FONT, fontInfoJson)
                        .add(JSON_FILL_COLOR, fillColorJson).build();
    //		    .add(JSON_OUTLINE_COLOR, outlineColorJson)
    //		    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
                
            }
            //if it is not DraggableText
            else if(node instanceof DraggableRectangle){
                Shape shape = (Shape)node;
                Draggable draggableShape = ((Draggable)shape);
                String order = draggableShape.getOrder();
                String name  = draggableShape.getName();
                String type  = draggableShape.getType();
                double x = draggableShape.getX();
                double y = draggableShape.getY();
                double width = draggableShape.getWidth();
                double height = draggableShape.getHeight();
                double arcwidth = ((DraggableRectangle)draggableShape).getArcWidth();
                double archeight = ((DraggableRectangle)draggableShape).getArcHeight();
                double borderThickness = shape.getStrokeWidth();
                JsonObject StrokeColorJson = makeJsonColorObject((Color)shape.getStroke());
                RadialGradient ori = (RadialGradient)shape.getFill();
                double focusAngle = ((RadialGradient)(shape.getFill())).getFocusAngle();
                double focusDistance = ((RadialGradient)shape.getFill()).getFocusDistance();
                double cgcenterX = ((RadialGradient)shape.getFill()).getCenterX();
                double cgcenterY = ((RadialGradient)shape.getFill()).getCenterY();
                double cgradius = ((RadialGradient)shape.getFill()).getRadius();
                CycleMethod cycleMethod = ((RadialGradient)shape.getFill()).getCycleMethod();
                Color Stop0Color = ((RadialGradient)shape.getFill()).getStops().get(0).getColor();
                Color Stop1Color = ((RadialGradient)shape.getFill()).getStops().get(1).getColor();;
                JsonObject Stop0ColorColorJson = makeJsonColorObject(Stop0Color);
                JsonObject Stop1ColorColorJson = makeJsonColorObject(Stop1Color);
                //JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());


                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, order)
                        .add(JSON_NAME, name)
                        .add(JSON_TYPE, type)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_ARCWIDTH,arcwidth)
                        .add(JSON_ARCHEIGHT,archeight)
                        .add(JSON_STROKEWIDTH, borderThickness)
                        .add(JSON_STROKE, StrokeColorJson)
                        .add(JSON_FOCUSANGLE, focusAngle)
                        .add(JSON_FOCUSDISTANCE, focusDistance)
                        .add(JSON_CGCENTERX, cgcenterX)
                        .add(JSON_CGCENTERY, cgcenterY)
                        .add(JSON_CGRADIUS , cgradius)
                        .add(JSON_CYCLEMETHOD , cycleMethod.toString())
                        .add(JSON_STOP0COLOR, Stop0ColorColorJson)
                        .add(JSON_STOP1COLOR, Stop1ColorColorJson)
                        //.add(JSON_FILL_COLOR, fillColorJson)
                        .build();
    //		    .add(JSON_OUTLINE_COLOR, outlineColorJson)
    //		    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
            }
            else if(node instanceof DraggableTriangle){
                Shape shape = (Shape)node;
                Draggable draggableShape = ((Draggable)shape);
                String order = draggableShape.getOrder();
                String name  = draggableShape.getName();
                String type  = draggableShape.getType();
                ArrayList<Double> points = ((DraggableTriangle)draggableShape).getAllPoints();
                double point1_x=points.get(0);double point1_y=points.get(1);
                double point2_x=points.get(2);double point2_y=points.get(3);
                double point3_x=points.get(4);double point3_y=points.get(5);
                double borderThickness = shape.getStrokeWidth();
                JsonObject StrokeColorJson = makeJsonColorObject((Color)shape.getStroke());
                RadialGradient ori = (RadialGradient)shape.getFill();
                double focusAngle = ((RadialGradient)(shape.getFill())).getFocusAngle();
                double focusDistance = ((RadialGradient)shape.getFill()).getFocusDistance();
                double cgcenterX = ((RadialGradient)shape.getFill()).getCenterX();
                double cgcenterY = ((RadialGradient)shape.getFill()).getCenterY();
                double cgradius = ((RadialGradient)shape.getFill()).getRadius();
                CycleMethod cycleMethod = ((RadialGradient)shape.getFill()).getCycleMethod();
                Color Stop0Color = ((RadialGradient)shape.getFill()).getStops().get(0).getColor();
                Color Stop1Color = ((RadialGradient)shape.getFill()).getStops().get(1).getColor();;
                JsonObject Stop0ColorColorJson = makeJsonColorObject(Stop0Color);
                JsonObject Stop1ColorColorJson = makeJsonColorObject(Stop1Color);
                //JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());


                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, order)
                        .add(JSON_NAME, name)
                        .add(JSON_TYPE, type)
                        .add(JSON_POINT1X, point1_x)
                        .add(JSON_POINT1Y, point1_y)
                        .add(JSON_POINT2X, point2_x)
                        .add(JSON_POINT2Y, point2_y)
                        .add(JSON_POINT3X, point3_x)
                        .add(JSON_POINT3Y, point3_y)
                        .add(JSON_STROKEWIDTH, borderThickness)
                        .add(JSON_STROKE, StrokeColorJson)
                        .add(JSON_FOCUSANGLE, focusAngle)
                        .add(JSON_FOCUSDISTANCE, focusDistance)
                        .add(JSON_CGCENTERX, cgcenterX)
                        .add(JSON_CGCENTERY, cgcenterY)
                        .add(JSON_CGRADIUS , cgradius)
                        .add(JSON_CYCLEMETHOD , cycleMethod.toString())
                        .add(JSON_STOP0COLOR, Stop0ColorColorJson)
                        .add(JSON_STOP1COLOR, Stop1ColorColorJson)
                        //.add(JSON_FILL_COLOR, fillColorJson)
                        .build();
    //		    .add(JSON_OUTLINE_COLOR, outlineColorJson)
    //		    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
            }
            else if(node instanceof DraggableImage){
                Draggable draggableShape = ((DraggableImage)node);
                String order = draggableShape.getOrder();
                String name  = draggableShape.getName();
                String type  = draggableShape.getType();
                double x = draggableShape.getX();
                double y = draggableShape.getY();

                String filepath = ((DraggableImage)draggableShape).resource.getAbsolutePath();
                
                //URL imagePath = ((DraggableImage)draggableShape).getImage().getClass().getResource(name);
//                String text = ((DraggableText)draggableShape).getText();
//                //String font = ((DraggableText)draggableShape).getFont().toString();
//                JsonObject fontInfoJson  = makeJsonFontObject(((DraggableText)draggableShape).getFont());
//                JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());

                //JsonObject outlineColorJson = makeJsonColorObject((Color)shape.getStroke());
                //double outlineThickness = shape.getStrokeWidth();

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, order)
                        .add(JSON_NAME, name)
                        .add(JSON_TYPE, type)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_FILEPATH, filepath)

//                        .add(JSON_TEXT, text)
//                        .add(JSON_FONT, fontInfoJson)
//                        .add(JSON_FILL_COLOR, fillColorJson)
                        .build();
    //		    .add(JSON_OUTLINE_COLOR, outlineColorJson)
    //		    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
                
            }
            else if(node instanceof DraggableCircle){
                Shape shape = (Shape)node;
                Draggable draggableShape = ((Draggable)shape);
                String order = draggableShape.getOrder();
                String name  = draggableShape.getName();
                String type  = draggableShape.getType();
                double centerX = ((DraggableCircle)draggableShape).getCenterX();
                double centerY = ((DraggableCircle)draggableShape).getCenterY();
                double radius = ((DraggableCircle)draggableShape).getRadius();
                double borderThickness = shape.getStrokeWidth();
                JsonObject StrokeColorJson = makeJsonColorObject((Color)shape.getStroke());
                //JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());
                RadialGradient ori = (RadialGradient)shape.getFill();
                double focusAngle = ((RadialGradient)(shape.getFill())).getFocusAngle();
                double focusDistance = ((RadialGradient)shape.getFill()).getFocusDistance();
                double cgcenterX = ((RadialGradient)shape.getFill()).getCenterX();
                double cgcenterY = ((RadialGradient)shape.getFill()).getCenterY();
                double cgradius = ((RadialGradient)shape.getFill()).getRadius();
                CycleMethod cycleMethod = ((RadialGradient)shape.getFill()).getCycleMethod();
                Color Stop0Color = ((RadialGradient)shape.getFill()).getStops().get(0).getColor();
                Color Stop1Color = ((RadialGradient)shape.getFill()).getStops().get(1).getColor();;
                JsonObject Stop0ColorColorJson = makeJsonColorObject(Stop0Color);
                JsonObject Stop1ColorColorJson = makeJsonColorObject(Stop1Color);

                 


                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_ORDER, order)
                        .add(JSON_NAME, name)
                        .add(JSON_TYPE, type)
                        .add(JSON_CENTERX, centerX)
                        .add(JSON_CENTERY, centerY)
                        .add(JSON_RADIUS, radius)
                        .add(JSON_STROKEWIDTH, borderThickness)
                        .add(JSON_STROKE, StrokeColorJson)
                        .add(JSON_FOCUSANGLE, focusAngle)
                        .add(JSON_FOCUSDISTANCE, focusDistance)
                        .add(JSON_CGCENTERX, cgcenterX)
                        .add(JSON_CGCENTERY, cgcenterY)
                        .add(JSON_CGRADIUS , cgradius)
                        .add(JSON_CYCLEMETHOD , cycleMethod.toString())
                        .add(JSON_STOP0COLOR, Stop0ColorColorJson)
                        .add(JSON_STOP1COLOR, Stop1ColorColorJson)
                        
                        
                        
                        
                        
                        .build();
    //		    .add(JSON_OUTLINE_COLOR, outlineColorJson)
    //		    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
            }

	    
	}
        JsonArray shapesArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_SHAPES, shapesArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
	//JsonArray itemsArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	//JsonObject toDoDataJSO = Json.createObjectBuilder()
//		.add(JSON_NAME, name)
//                .add(JSON_OWNER, owner)
//		.add(JSON_ITEMS, itemsArray)
//		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
//	Map<String, Object> properties = new HashMap<>(1);
//	properties.put(JsonGenerator.PRETTY_PRINTING, true);
//	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
//	StringWriter sw = new StringWriter();
//	JsonWriter jsonWriter = writerFactory.createWriter(sw);
//	jsonWriter.writeObject(toDoDataJSO);
//	jsonWriter.close();
//
//	// INIT THE WRITER
//	OutputStream os = new FileOutputStream(filePath);
//	JsonWriter jsonFileWriter = Json.createWriter(os);
//	jsonFileWriter.writeObject(toDoDataJSO);
//	String prettyPrinted = sw.toString();
//	PrintWriter pw = new PrintWriter(filePath);
//	pw.write(prettyPrinted);
//	pw.close();
    }
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error
     * reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	goLogoloData toDoData = (goLogoloData)data;
	toDoData.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// LOAD THE BACKGROUND COLOR
	//Color bgColor = loadColor(json, JSON_BG_COLOR);
	//toDoData.setBackgroundColor(bgColor);
	
	// AND NOW LOAD ALL THE SHAPES
	JsonArray jsonShapeArray = json.getJsonArray(JSON_SHAPES);
	for (int i = 0; i < jsonShapeArray.size(); i++) {
	    JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
	    Node shape = loadShape(jsonShape);
	    toDoData.addItem(shape);
	}
	
	// LOAD LIST NAME AND OWNER
//	String name = json.getString(JSON_NAME);
//	String owner = json.getString(JSON_OWNER);
//        toDoData.setName(name);
//        toDoData.setOwner(owner);
	
	// AND NOW LOAD ALL THE ITEMS
//	JsonArray jsonItemArray = json.getJsonArray(JSON_ITEMS);
//	for (int i = 0; i < jsonItemArray.size(); i++) {
//	    JsonObject jsonItem = jsonItemArray.getJsonObject(i);
//	    Node item = loadItem(jsonItem);
//	    toDoData.addItem(item);
//	}
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public Node loadItem(JsonObject jsonItem) {
	// GET THE DATA
	String category = jsonItem.getString(JSON_ORDER);
	String description = jsonItem.getString(JSON_NAME);
        String startDateText = jsonItem.getString(JSON_TYPE);

        
	// THEN USE THE DATA TO BUILD AN ITEM
        //Node item = new Node(category, description, startDate, endDate, assignedTo, completed);
        
	// ALL DONE, RETURN IT
        return null;
	//return item;
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method would be used to export data to another format,
     * which we're not doing in this assignment.
     */
    @Override
    public void exportData(AppDataComponent data, String savedFileName) throws IOException {
        
//        String toDoListName = savedFileName.substring(0, savedFileName.indexOf("."));
//        String fileToExport = toDoListName + ".html";
//        try {
//            // GET THE ACTUAL DATA
//            goLogoloData toDoData = (goLogoloData)data;
//            PropertiesManager props = PropertiesManager.getPropertiesManager();
//            String exportDirPath = props.getProperty(APP_PATH_EXPORT) + toDoListName + "/";
//            File exportDir = new File(exportDirPath);
//            if (!exportDir.exists()) {
//                exportDir.mkdir();
//            }
//
//            // NOW LOAD THE TEMPLATE DOCUMENT
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//            String htmlTemplatePath = props.getPropertiesDataPath() + props.getProperty(GLGL_EXPORT_TEMPLATE_FILE_NAME);
//            File file = new File(htmlTemplatePath);
//            System.out.println(file.getPath() + " exists? " + file.exists());
//            URL templateURL = file.toURI().toURL();
//            Document exportDoc = docBuilder.parse(templateURL.getPath());
//
//            // SET THE WEB PAGE TITLE
//            Node titleNode = exportDoc.getElementsByTagName(TITLE_TAG).item(0);
//            titleNode.setTextContent(toDoData.getName());
//
//            // SET THE NAME
//            Node nameNode = getNodeWithId(exportDoc, HTML.Tag.TD.toString(), NAME_TAG);
//            nameNode.setTextContent(toDoData.getName());
//
//            // SET THE OWNER
//            Node ownerNode = getNodeWithId(exportDoc, HTML.Tag.TD.toString(), OWNER_TAG);
//            ownerNode.setTextContent(toDoData.getOwner());
//            
//            // ADD ALL THE ITEMS
//            Node tDataNode = getNodeWithId(exportDoc, "tdata", TABLE_DATA_TAG);
//            Iterator<Node> itemsIt = toDoData.itemsIterator();
//            while (itemsIt.hasNext()) {
//                Node item = itemsIt.next();
//                Element trElement = exportDoc.createElement(HTML.Tag.TR.toString());
//                tDataNode.appendChild(trElement);
////                addCellToRow(exportDoc, trElement, item.getCategory());
////                addCellToRow(exportDoc, trElement, item.getDescription());
////                addCellToRow(exportDoc, trElement, item.getStartDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
////                addCellToRow(exportDoc, trElement, item.getEndDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
////                addCellToRow(exportDoc, trElement, item.getAssignedTo());
////                addCellToRow(exportDoc, trElement, "" + item.isCompleted());
//            }
//            
//            // CORRECT THE APP EXPORT PAGE
//            props.addProperty(APP_EXPORT_PAGE, exportDirPath + fileToExport);
//
//            // EXPORT THE WEB PAGE
//            saveDocument(exportDoc, props.getProperty(APP_EXPORT_PAGE));
//        }
//        catch(SAXException | ParserConfigurationException
//                | TransformerException exc) {
//            throw new IOException("Error loading " + fileToExport);
//        }
    }
    private void addCellToRow(Document doc, Node rowNode, String text) {
//        Element tdElement = doc.createElement(HTML.Tag.TD.toString());
//        tdElement.setTextContent(text);
//        rowNode.appendChild(tdElement);
    }
    private Node getNodeWithId(Document doc, String tagType, String searchID) {
//        NodeList testNodes = doc.getElementsByTagName(tagType);
//        for (int i = 0; i < testNodes.getLength(); i++) {
//            Node testNode = testNodes.item(i);
//            Node testAttr = testNode.getAttributes().getNamedItem(HTML.Attribute.ID.toString());
//            if ((testAttr != null) && testAttr.getNodeValue().equals(searchID)) {
//                return testNode;
//            }
//        }
        return null;
    }
    private void saveDocument(Document doc, String outputFilePath)
            throws TransformerException, TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Result result = new StreamResult(new File(outputFilePath));
        Source source = new DOMSource(doc);
        transformer.transform(source, result);
    }

    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
    
    private JsonObject makeJsonColorObject(Color color) {
	JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }
    private JsonObject makeJsonFontObject(Font font) {
	JsonObject fontJson = Json.createObjectBuilder()
                .add(JSON_FONTNAME, font.getName())
		.add(JSON_FONTFAMILY, font.getFamily())
		.add(JSON_FONTSTYLE, font.getStyle())
		.add(JSON_FONTSIZE, font.getSize()).build();	
	return fontJson;
    }
   
    
    private Node loadShape(JsonObject jsonShape) throws IOException {
	// FIRST BUILD THE PROPER SHAPE TYPE
	String type = jsonShape.getString(JSON_TYPE);
        String name = jsonShape.getString(JSON_NAME);
        String order= jsonShape.getString(JSON_ORDER);
	Shape shape ;
	if (type.equals("Rect")) {
	    shape = new DraggableRectangle();
            ((Draggable)shape).setOrder(order);
            ((Draggable)shape).setName(name);
            ((Draggable)shape).setType(type);
            //Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color strokeColor = loadColor(jsonShape, JSON_STROKE);
            double focusAngle = getDataAsDouble(jsonShape, JSON_FOCUSANGLE);
            double focusDistance = getDataAsDouble(jsonShape, JSON_FOCUSDISTANCE);
            double cgcenterX = getDataAsDouble(jsonShape, JSON_CGCENTERX);
            double cgcenterY = getDataAsDouble(jsonShape, JSON_CGCENTERY);
            double cgradius = getDataAsDouble(jsonShape, JSON_CGRADIUS);
            String cycle = jsonShape.get(JSON_CYCLEMETHOD).toString();
            cycle = cycle.substring(1, cycle.length()-1);
            CycleMethod cycleMethod =CycleMethod.valueOf(cycle);
            Color Stop0Color = loadColor(jsonShape, JSON_STOP0COLOR);
            Color Stop1Color = loadColor(jsonShape, JSON_STOP1COLOR);
            RadialGradient newRadialGradient = new RadialGradient(focusAngle,focusDistance,cgcenterX,cgcenterY,cgradius,true,cycleMethod,new Stop[]{new Stop(0,  Stop0Color), new Stop(1.0,  Stop1Color)});

            //Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            //double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(newRadialGradient);
            shape.setStroke(strokeColor);
            //shape.setStroke(outlineColor);
            //shape.setStrokeWidth(outlineThickness);

            // AND THEN ITS DRAGGABLE PROPERTIES
            
            //                                    .add(JSON_ARCWIDTH,arcwidth)
//                        .add(JSON_ARCHEIGHT,archeight)
//                        .add(JSON_STROKEWIDTH, borderThickness)
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            double arcwidth = getDataAsDouble(jsonShape, JSON_ARCWIDTH);
            double archeight = getDataAsDouble(jsonShape, JSON_ARCHEIGHT);
            double strokewidth = getDataAsDouble(jsonShape, JSON_STROKEWIDTH);
            Draggable draggableShape = (Draggable)shape;
            draggableShape.setLocationAndSize(x, y, width, height);
            ((DraggableRectangle)draggableShape).setArcWidth(arcwidth);
            ((DraggableRectangle)draggableShape).setArcHeight(archeight);
            ((DraggableRectangle)draggableShape).setStrokeWidth(strokewidth);

            // ALL DONE, RETURN IT
            return shape;
	}
        else if (type.equals("Image")) {
            DraggableImage draggableimage = new DraggableImage();
            draggableimage.setOrder(order);
            draggableimage.setName(name);
            draggableimage.setType(type);
            
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            String path = jsonShape.get(JSON_FILEPATH).toString();
            path = path.substring(1,path.length()-1);
            File imageFile = new File(path);
            BufferedImage bufferedImage = ImageIO.read(imageFile);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            draggableimage.setImage(image);
            draggableimage.setX(x);
            draggableimage.setY(y);
            draggableimage.resource=imageFile;
            return draggableimage;
        }
        else if (type.equals("Circle")) {
	    shape = new DraggableCircle();
            ((Draggable)shape).setOrder(order);
            ((Draggable)shape).setName(name);
            ((Draggable)shape).setType(type);

            Color strokeColor = loadColor(jsonShape, JSON_STROKE);
            
            double focusAngle = getDataAsDouble(jsonShape, JSON_FOCUSANGLE);
            double focusDistance = getDataAsDouble(jsonShape, JSON_FOCUSDISTANCE);
            double cgcenterX = getDataAsDouble(jsonShape, JSON_CGCENTERX);
            double cgcenterY = getDataAsDouble(jsonShape, JSON_CGCENTERY);
            double cgradius = getDataAsDouble(jsonShape, JSON_CGRADIUS);
            String cycle = jsonShape.get(JSON_CYCLEMETHOD).toString();
            cycle = cycle.substring(1, cycle.length()-1);
            CycleMethod cycleMethod =CycleMethod.valueOf(cycle);
            Color Stop0Color = loadColor(jsonShape, JSON_STOP0COLOR);
            Color Stop1Color = loadColor(jsonShape, JSON_STOP1COLOR);
            RadialGradient newRadialGradient = new RadialGradient(focusAngle,focusDistance,cgcenterX,cgcenterY,cgradius,true,cycleMethod,new Stop[]{new Stop(0,  Stop0Color), new Stop(1.0,  Stop1Color)});

            //Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            //double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(newRadialGradient);
            shape.setStroke(strokeColor);
            //shape.setStroke(outlineColor);
            //shape.setStrokeWidth(outlineThickness);

            // AND THEN ITS DRAGGABLE PROPERTIES
            
            //                                    .add(JSON_ARCWIDTH,arcwidth)
//                        .add(JSON_ARCHEIGHT,archeight)
//                        .add(JSON_STROKEWIDTH, borderThickness)
            double centerX = getDataAsDouble(jsonShape, JSON_CENTERX);
            double centerY = getDataAsDouble(jsonShape, JSON_CENTERY);
            double radius = getDataAsDouble(jsonShape, JSON_RADIUS);
            double strokewidth = getDataAsDouble(jsonShape, JSON_STROKEWIDTH);
            Draggable draggableShape = (Draggable)shape;
            //draggableShape.setLocationAndSize(x, y, width, height);
            ((DraggableCircle)draggableShape).setCenterX(centerX);
            ((DraggableCircle)draggableShape).setCenterY(centerY);
            ((DraggableCircle)draggableShape).setRadius(radius);
            ((DraggableCircle)draggableShape).setStrokeWidth(strokewidth);

            // ALL DONE, RETURN IT
            return shape;
	}
	else if (type.equals("Text")){
	    shape = new DraggableText();
            ((Draggable)shape).setOrder(order);
            ((Draggable)shape).setName(name);
            ((Draggable)shape).setType(type);
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            //Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            //double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(fillColor);
            //shape.setStroke(outlineColor);
            //shape.setStrokeWidth(outlineThickness);

            // AND THEN ITS DRAGGABLE PROPERTIES
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            String text = jsonShape.get(JSON_TEXT).toString();
            text = text.substring(1, text.length()-1);
            
            Font font = loadFont(jsonShape, JSON_FONT);
            ((DraggableText)(shape)).setText(text);
            

            ((DraggableText)(shape)).setFont(font);
            //Draggable draggableShape = (Draggable)shape;
            ((Draggable)shape).setLocationAndSize(x, y, width, height);

            // ALL DONE, RETURN IT
            return shape;
	}
        else if (type.equals("Triangle")){
            shape = new DraggableTriangle();
            ((Draggable)shape).setOrder(order);
            ((Draggable)shape).setName(name);
            ((Draggable)shape).setType(type);
            //Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color strokeColor = loadColor(jsonShape, JSON_STROKE);
            double focusAngle = getDataAsDouble(jsonShape, JSON_FOCUSANGLE);
            double focusDistance = getDataAsDouble(jsonShape, JSON_FOCUSDISTANCE);
            double cgcenterX = getDataAsDouble(jsonShape, JSON_CGCENTERX);
            double cgcenterY = getDataAsDouble(jsonShape, JSON_CGCENTERY);
            double cgradius = getDataAsDouble(jsonShape, JSON_CGRADIUS);
            String cycle = jsonShape.get(JSON_CYCLEMETHOD).toString();
            cycle = cycle.substring(1, cycle.length()-1);
            CycleMethod cycleMethod =CycleMethod.valueOf(cycle);
            Color Stop0Color = loadColor(jsonShape, JSON_STOP0COLOR);
            Color Stop1Color = loadColor(jsonShape, JSON_STOP1COLOR);
            RadialGradient newRadialGradient = new RadialGradient(focusAngle,focusDistance,cgcenterX,cgcenterY,cgradius,true,cycleMethod,new Stop[]{new Stop(0,  Stop0Color), new Stop(1.0,  Stop1Color)});

            //Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            //double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(newRadialGradient);
            shape.setStroke(strokeColor);
            double point1_x = getDataAsDouble(jsonShape, JSON_POINT1X);
            double point1_y = getDataAsDouble(jsonShape, JSON_POINT1Y);
            double point2_x = getDataAsDouble(jsonShape, JSON_POINT2X);
            double point2_y = getDataAsDouble(jsonShape, JSON_POINT2Y);
            double point3_x = getDataAsDouble(jsonShape, JSON_POINT3X);
            double point3_y = getDataAsDouble(jsonShape, JSON_POINT3Y);
            ((DraggableTriangle)(shape)).getPoints().clear();
            ((DraggableTriangle)(shape)).getPoints().addAll(new Double[]{
                point1_x, point1_y,
                point2_x, point2_y,
                point3_x, point3_y });
//                        .add(JSON_ORDER, order)
//                        .add(JSON_NAME, name)
//                        .add(JSON_TYPE, type)
//                        .add(JSON_POINT1X, point1_x)
//                        .add(JSON_POINT1Y, point1_y)
//                        .add(JSON_POINT2X, point2_x)
//                        .add(JSON_POINT2Y, point2_y)
//                        .add(JSON_POINT3X, point3_x)
//                        .add(JSON_POINT3Y, point3_y)
//                        .add(JSON_STROKEWIDTH, borderThickness)
//                        .add(JSON_STROKE, StrokeColorJson)
//                        .add(JSON_FILL_COLOR, fillColorJson).build();
            //shape.setStroke(outlineColor);
            //shape.setStrokeWidth(outlineThickness);

            // AND THEN ITS DRAGGABLE PROPERTIES
            
            //                                    .add(JSON_ARCWIDTH,arcwidth)
//                        .add(JSON_ARCHEIGHT,archeight)
//                        .add(JSON_STROKEWIDTH, borderThickness)


            double strokewidth = getDataAsDouble(jsonShape, JSON_STROKEWIDTH);
//            Draggable draggableShape = (Draggable)shape;

            ((DraggableTriangle)(shape)).setStrokeWidth(strokewidth);

            // ALL DONE, RETURN IT
            return shape;
        }
        else{
            return null;
        }
	
	// THEN LOAD ITS FILL AND OUTLINE PROPERTIES
        
    }
    
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    private Font loadFont(JsonObject json, String fontToGet) {
	JsonObject jsonFont = json.getJsonObject(fontToGet);
        //String fontName = jsonFont.get(JSON_FONTNAME).toString();
	String fontFamily = jsonFont.get(JSON_FONTFAMILY).toString().substring(1,jsonFont.get(JSON_FONTFAMILY).toString().length()-1);
        String fontStyle = jsonFont.get(JSON_FONTSTYLE).toString();
        String fontweight;
        String fontposture;
        if(fontStyle.contains("Bold")){
            fontweight = "Bold";
        }
        else{
            fontweight = "Normal";
        }
        if(fontStyle.contains("Italic")){
            fontposture = "Italic";
        }
        else{
            fontposture = "Normal";
        }
            
            
        FontWeight weight= FontWeight.findByName(fontweight);
        FontPosture posture= FontPosture.findByName(fontposture);
        String fontSize = jsonFont.get(JSON_FONTSIZE).toString();
	//Font loadedColor = new Font(fontFamily, fontName, fontSize, fontStyle);
        Font loadedFont = Font.font(fontFamily, weight, posture, Double.parseDouble(fontSize));
	return loadedFont;
    }
}
