package glgl.data;

import javafx.beans.property.StringProperty;
import javafx.scene.Node;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public interface Draggable {
    public static final String RECTANGLE = "RECTANGLE";
    public static final String ELLIPSE = "ELLIPSE";
    public static final String IMAGE = "IMAGE";
    public static final String TEXT = "TEXT";
    public static final String TRIANGLE = "Triangle";
    //public golState getStartingState();
    public void start(int x, int y);
    public void drag(int x, int y);
    public void size(int x, int y);
    public double getX();
    public double getY();
    public double getWidth();
    public double getHeight();
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight);
    public int isXYonBoarder(int x,int y);
    public String getShapeType();
    public Node copyNode();
    public void setType(String type);
    public void setOrder(String type);
    public void setName(String type);
    public String getType();
    public String getOrder();
    public String getName();
    public StringProperty typeProperty();
    public StringProperty orderProperty();
    public StringProperty nameProperty();
    
}
