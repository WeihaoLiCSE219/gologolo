/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data;

import static glgl.data.Draggable.TEXT;
import static glgl.data.DraggableRectangle.DEFAULT_NAME;
import static glgl.data.DraggableRectangle.DEFAULT_ORDER;
import static glgl.data.DraggableRectangle.DEFAULT_TYPE;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.text.Text;

/**
 *
 * @author brandonchan
 */
public class DraggableText extends Text implements Draggable {

    double startX;
    double startY;
    final StringProperty order;
    final StringProperty name;
    final StringProperty type;
    public static final String DEFAULT_ORDER = "order";
    public static final String DEFAULT_NAME = "Rect_name";
    public static final String DEFAULT_TYPE = "Text";
    

    public DraggableText() {
        setX(0.0);
        setY(0.0);
        setWrappingWidth(0.0);
        setLineSpacing(0.0);
        setOpacity(1.0);
        startX = 0.0;
        startY = 0.0;
        startY = 0.0;
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);

    }

    public DraggableText copyNode() {
        DraggableText target = new DraggableText();
        target.startX = this.startX;
        target.startY = this.startY;
        target.setWrappingWidth(this.getWrappingWidth());
        target.setLineSpacing(this.getLineSpacing());
        target.setX(this.getX() + 10);
        target.setY(this.getY() + 10);
        target.setOpacity(this.getOpacity());
        target.setText(this.getText());
        target.setFont(this.getFont());
        target.setOrder(this.getOrder());
        target.setName(this.getName());
        target.setType(this.getType());
        return target;

    }

//    @Override
//    public golState getStartingState() {
//        return golState.SELECTING_SHAPE;
//    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(x);
        setY(y);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - (getX() + (getWidth() / 2));
        double diffY = y - (getY() + (getHeight() / 2));
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        xProperty().set(newX);
        yProperty().set(newY);
        startX = x;
        startY = y;
    }
    public String getOrder() {
        return order.get();
    }
    public void setOrder(String value) {
        order.set(value);
    }
    public StringProperty orderProperty(){
        return order;
    }
        public String getName() {
        return name.get();
    }
    public void setName(String value) {
        name.set(value);
    }
    public StringProperty nameProperty(){
        return name;
    }
        public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }
    public StringProperty typeProperty(){
        return type;
    }
    @Override
    public void size(int x, int y) {

    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
        yProperty().set(initY);

    }

    @Override
    public String getShapeType() {
        return TEXT;
    }

    @Override
    public double getWidth() {
        return getWrappingWidth();
    }

    @Override
    public double getHeight() {
        return getLineSpacing();
    }

    @Override
    public int isXYonBoarder(int x, int y) {
        return 0;
    }

}
