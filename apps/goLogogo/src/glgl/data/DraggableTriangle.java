package glgl.data;


import static glgl.data.DraggableRectangle.DEFAULT_FILL;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;


public class DraggableTriangle extends Polygon implements Draggable {

        final StringProperty order;
        final StringProperty name;
        final StringProperty type;
        public static final String DEFAULT_ORDER = "order";
        public static final String DEFAULT_NAME = "Triangle_name";
        public static final String DEFAULT_TYPE = "Triangle";

    public DraggableTriangle() {
        this.getPoints().addAll(new Double[]{
            100.0, 100.0,
            100.0, 300.0,
            300.0, 200.0 });
        setOpacity(1.0);
        setStroke(Color.YELLOW);
        setStrokeWidth(0);
        setFill(DEFAULT_FILL);
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    @Override
    public DraggableTriangle copyNode() {
        DraggableTriangle cloneTriangle = new DraggableTriangle();
        cloneTriangle.getPoints().clear();
        cloneTriangle.getPoints().addAll(this.getPoints());
        cloneTriangle.setOpacity(getOpacity());
        cloneTriangle.setFill(getFill());
        cloneTriangle.setStroke(getStroke());
        cloneTriangle.setStrokeWidth(getStrokeWidth());
        return cloneTriangle;
    }
        @Override
    public void start(int x, int y) {

    }
        @Override
    public void drag(int x, int y) {
//	double diffX = x - startCenterX;
//	double diffY = y - startCenterY;
//	double newX = getCenterX() + diffX;
//	double newY = getCenterY() + diffY;
        ArrayList<Double> points = this.getAllPoints();
        double point1_x=points.get(0);double point1_y=points.get(1);
        double point2_x=points.get(2);double point2_y=points.get(3);
        double point3_x=points.get(4);double point3_y=points.get(5);
        double centerX = (point1_x+point2_x+point3_x)/3;
        double centerY = (point1_y+point2_y+point3_y)/3;
        double diffX = x-centerX;
        double diffY = y-centerY;
        this.getPoints().clear();
        this.getPoints().addAll(new Double[]{
            point1_x+diffX, point1_y+diffY,
            point2_x+diffX, point2_y+diffY,
            point3_x+diffX, point3_y+diffY });
    }
    public void size(int x, int y) {
//	double width = x - startCenterX;
//	double height = y - startCenterY;
//	double centerX = startCenterX + (width / 2);
//	double centerY = startCenterY + (height / 2);
//	setCenterX(centerX);
//	setCenterY(centerY);
//	setRadius(width / 2);
    }
        @Override
    public double getX() {
	//return getCenterX() - getRadius();
        return 0;
    }

    @Override
    public double getY() {
	//return getCenterY() - getRadius();
        return 0;
    }

    @Override
    public double getWidth() {
	//return getRadius() * 2;
        return 0;
    }

    @Override
    public double getHeight() {
	//return getRadius() * 2;
        return 0;
    }
    public ArrayList<Double> getAllPoints(){ 
        double point1_x=this.getPoints().get(0);double point1_y=this.getPoints().get(1);
        double point2_x=this.getPoints().get(2);double point2_y=this.getPoints().get(3);
        double point3_x=this.getPoints().get(4);double point3_y=this.getPoints().get(5);
        ArrayList<Double> x=new ArrayList<>();
        x.add(point1_x);x.add(point1_y);x.add(point2_x);x.add(point2_y);x.add(point3_x);x.add(point3_y);
        return x;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        
    }
        @Override
    public String getShapeType() {
        return TRIANGLE;    
    }
    public String getOrder() {
        return order.get();
    }
    public void setOrder(String value) {
        order.set(value);
    }
    public StringProperty orderProperty(){
        return order;
    }
        public String getName() {
        return name.get();
    }
    public void setName(String value) {
        name.set(value);
    }
    public StringProperty nameProperty(){
        return name;
    }
        public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }
    public StringProperty typeProperty(){
        return type;
    }

    @Override
    public int isXYonBoarder(int x, int y) {
        ArrayList<Double> points = this.getAllPoints();
        double point1_x=points.get(0);double point1_y=points.get(1);
        double point2_x=points.get(2);double point2_y=points.get(3);
        double point3_x=points.get(4);double point3_y=points.get(5);
        if(x>=point1_x-20 && x<=point1_x+20 && y>=point1_y-20 && y<=point1_y+20) return 1;
        else if(x>=point2_x-20 && x<=point2_x+20 && y>=point2_y-20 && y<=point2_y+20) return 2;
        else if(x>=point3_x-20 && x<=point3_x+20 && y>=point3_y-20 && y<=point3_y+20) return 3;
        return 0;    
    }
    
    public void sizeDepOnPos(int x, int y,int Pos){
        if(Pos==1){
            ArrayList<Double> points = this.getAllPoints();
            double point2_x=points.get(2);double point2_y=points.get(3);
            double point3_x=points.get(4);double point3_y=points.get(5);
            this.getPoints().clear();
            this.getPoints().addAll(new Double[]{
            (double)x,(double)y,
            point2_x, point2_y,
            point3_x, point3_y });
        }    
        if(Pos==2){
            ArrayList<Double> points = this.getAllPoints();
            double point1_x=points.get(0);double point1_y=points.get(1);
            double point2_x=points.get(2);double point2_y=points.get(3);
            double point3_x=points.get(4);double point3_y=points.get(5);
            this.getPoints().clear();
            this.getPoints().addAll(new Double[]{
            point1_x, point1_y,
            (double)x,(double)y,
            //point2_x, point2_y,
            point3_x, point3_y });
        }  
        if(Pos==3){
            ArrayList<Double> points = this.getAllPoints();
            double point1_x=points.get(0);double point1_y=points.get(1);
            double point2_x=points.get(2);double point2_y=points.get(3);
            double point3_x=points.get(4);double point3_y=points.get(5);
            this.getPoints().clear();
            this.getPoints().addAll(new Double[]{
            point1_x, point1_y,
            point2_x, point2_y,
            (double)x,(double)y,});
        }        
        }
    }


