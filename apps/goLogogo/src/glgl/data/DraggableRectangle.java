package glgl.data;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

/**
 * This is a draggable rectangle for our goLogoLo application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */

public class DraggableRectangle extends Rectangle implements Draggable {

    double startX;
    double startY;
    final StringProperty order;
    final StringProperty name;
    final StringProperty type;
    public static final String DEFAULT_ORDER = "order";
    public static final String DEFAULT_NAME = "Rect_name";
    public static final String DEFAULT_TYPE = "Rect";
    
    public static final int DEFAULT_STROKEWIDTHHALF = 10;
    public static final RadialGradient DEFAULT_FILL= new RadialGradient(-20, 1, 0.5, 0.5, 0.6, true, CycleMethod.NO_CYCLE, new Stop[] {
        new Stop(0,  Color.TRANSPARENT),
        new Stop(1,  Color.BLANCHEDALMOND),
    });

    public DraggableRectangle() {
        setX(0.0);
        setY(0.0);
        setWidth(0.0);
        setHeight(0.0);
        setOpacity(1.0);
        setFill(DEFAULT_FILL);
        startX = 0.0;
        startY = 0.0;
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    public DraggableRectangle(double x, double y, double width, double height,String initOrder, String initName, String initType) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setOpacity(1.0);
        setFill(DEFAULT_FILL);
        startX = 0.0;
        startY = 0.0;
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    

    public DraggableRectangle copyNode() {
        DraggableRectangle target = new DraggableRectangle();
        target.startX = this.startX;
        target.startY = this.startY;
        target.setWidth(this.getWidth());
        target.setHeight(this.getHeight());
        target.setArcHeight(this.getArcHeight());
        target.setArcWidth(this.getArcWidth());
        target.setX(this.getX() + 10);
        target.setY(this.getY() + 10);
        target.setOpacity(this.getOpacity());
        target.setFill(this.getFill());
        target.setStroke(this.getStroke());
        target.setStrokeWidth(this.getStrokeWidth());
        target.setOrder(this.getOrder());
        target.setName(this.getName());
        target.setType(this.getType());
        
        return target;

    }
    public String getOrder() {
        return order.get();
    }
    public void setOrder(String value) {
        order.set(value);
    }
    public StringProperty orderProperty(){
        return order;
    }
        public String getName() {
        return name.get();
    }
    public void setName(String value) {
        name.set(value);
    }
    public StringProperty nameProperty(){
        return name;
    }
        public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }
    public StringProperty typeProperty(){
        return type;
    }

//    @Override
//    public golState getStartingState() {
//        return golState.STARTING_RECTANGLE;
//    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(x);
        setY(y);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - (getX() + (getWidth() / 2));
        double diffY = y - (getY() + (getHeight() / 2));
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        setX(newX);
	setY(newY);
//        startX = x;
//        startY = y;
        //this.setLocationAndSize(startX, startY, startX, startX);

//        xProperty().set(x);
//        yProperty().set(y);
//        startX = x;
//        startY = y;
    }

    public String cT(double x, double y) {
        return "(x,y): (" + x + "," + y + ")";
    }

    @Override
    public void size(int x, int y) {
        
        //rightup corner
//        if(x>this.getX() && y<this.getY()){
//            
//            double width = x - getX();
//            double height= getY()-y;
//            widthProperty().set(width);
//            heightProperty().set(height);
//            this.yProperty().set(y);
//        }
        double width = x - getX();
        double height = y - getY();
//        double width = x - startX;
//        double height = y - startY;
        widthProperty().set(width);
        heightProperty().set(height);
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
        yProperty().set(initY);
        widthProperty().set(initWidth);
        heightProperty().set(initHeight);
    }

    @Override
    public String getShapeType() {
        return RECTANGLE;
    }

    public int isXYonBoarder(int x,int y) {
        double a = this.getX();
        double b = this.getY();
        double c = this.getWidth();
        double d = this.getHeight();
        if((x>=(this.getX()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+DEFAULT_STROKEWIDTHHALF)){
            return 1;//left top corner
        }
        if((x>=(this.getX()+this.getWidth()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+this.getWidth()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+DEFAULT_STROKEWIDTHHALF)){
            return 2;//right top corner
        }
        if((x>=(this.getX()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+DEFAULT_STROKEWIDTHHALF)  && (y>=(this.getY()+this.getHeight()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+this.getHeight()+DEFAULT_STROKEWIDTHHALF)){
            return 3;//left down corner
        }
        if((x>=(this.getX()+this.getWidth()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+this.getWidth()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()+this.getHeight()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+this.getHeight()+DEFAULT_STROKEWIDTHHALF)){
            return 4;//right down corner
        }
        if((x>=(this.getX()+DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+this.getWidth()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+DEFAULT_STROKEWIDTHHALF)){
            return 5;//top boarder
        }
        if((x>=(this.getX()+DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+this.getWidth()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()+this.getHeight()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+this.getHeight()+DEFAULT_STROKEWIDTHHALF)){
            return 6;//down boarder
        }
        if((x>=(this.getX()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()+DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+this.getHeight()+DEFAULT_STROKEWIDTHHALF)){
            return 7;//left boarder
        }
        if((x>=(this.getX()+this.getWidth()-DEFAULT_STROKEWIDTHHALF) && x<=this.getX()+this.getWidth()+DEFAULT_STROKEWIDTHHALF) && (y>=(this.getY()-DEFAULT_STROKEWIDTHHALF) && y<=this.getY()+this.getHeight()+DEFAULT_STROKEWIDTHHALF)){
            return 8;//right boarder
        }
        return 0;
    }

    public void sizeDepOnPos(int x, int y,int Pos) {
        if(Pos==1){
            double width  = getWidth()+getX()-x;
            double height = getHeight()+getY()-y;
            setX(x);
	    setY(y);
            widthProperty().set(width);
            heightProperty().set(height);
            
        }
        if(Pos==2){
            double width  = x - getX();
            double height = getHeight()+getY()-y;
	    setY(y);
            widthProperty().set(width);
            heightProperty().set(height);
            
        }    
        if(Pos==3){
            double width  = getWidth()+getX()-x;
            double height = y - getY();
            setX(x);
            widthProperty().set(width);
            heightProperty().set(height);
        }
        if(Pos==4){
            double width = x - getX();
            double height = y - getY();
            widthProperty().set(width);
            heightProperty().set(height);
        }
        if(Pos==5){
            double height = getHeight()+getY()-y;
            setY(y);
            heightProperty().set(height);
        }     
        if(Pos==6){
            double height = y - getY();
            heightProperty().set(height);
        }     
        if(Pos==7){
            double width  = getWidth()+getX()-x;
            setX(x);
            widthProperty().set(width);
        }
        if(Pos==8){
            double width  = x - getX();
            widthProperty().set(width);
        }
    }

}
