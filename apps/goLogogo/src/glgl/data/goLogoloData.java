package glgl.data;

import static djf.AppPropertyType.APP_TEXTINPUT_CONTENT;
import static djf.AppPropertyType.APP_TEXTINPUT_TITLE;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import glgl.goLogoloApp;
import static glgl.goLogoloPropertyType.*;
import glgl.transactions.EditItem_Transaction;
import glgl.transactions.SizeCircle_Transaction;
import glgl.workspace.controllers.ItemsController;
import glgl.workspace.goLogoloWorkspace;
import java.util.Optional;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;

/**
 *
 * @author McKillaGorilla
 */
public class goLogoloData implements AppDataComponent {
    goLogoloApp app;
    ObservableList<Node> items;
    TableViewSelectionModel itemsSelectionModel;
    StringProperty nameProperty;
    StringProperty ownerProperty;
    
    ItemsController itemsController;
    
    public Draggable selectedDraggableShape;
    
    public goLogoloData(goLogoloApp initApp) {
        app = initApp;
        items = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        
        
        // GET ALL THE THINGS WE'LL NEED TO MANIUPLATE THE TABLE
       TableView tableView = (TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW);
//        TableView tableView = (TableView) app.getGUIModule().getGUINode(TDLM_ITEMS_TABLE_VIEW);
        tableView.setItems(items);
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.SINGLE);

//        
//        // AND FOR LIST NAME AND OWNER DATA
//        nameProperty = ((TextField)app.getGUIModule().getGUINode(TDLM_NAME_TEXT_FIELD)).textProperty();
//        ownerProperty = ((TextField)app.getGUIModule().getGUINode(TDLM_OWNER_TEXT_FIELD)).textProperty();
    }
    
    public String getName() {
        return nameProperty.getValue();
    }
    
    public String getOwner() {
        return ownerProperty.getValue();
    }
    
    public Iterator<Node> itemsIterator() {
        return this.items.iterator();
    }
    
    public void setName(String initName) {
        nameProperty.setValue(initName);
    }
    
    public void setOwner(String initOwner) {
        ownerProperty.setValue(initOwner);
    }

    @Override
    public void reset() {
        selectedDraggableShape=null;
        items.clear();
        //AppGUIModule gui = app.getGUIModule();
        
        // CLEAR OUT THE TEXT FIELDS
//        nameProperty.setValue("");
//        ownerProperty.setValue("");
        
        // CLEAR OUT THE ITEMS FROM THE TABLE
//        TableView tableView = (TableView)gui.getGUINode(TDLM_ITEMS_TABLE_VIEW);
//        items = tableView.getItems();
//        items.clear();
    }

    public boolean isItemSelected() {
        if(selectedDraggableShape==null){return false;}
        else return true;
    }
    
//    public boolean areItemsSelected() {
//        ObservableList<Node> selectedItems = this.getSelectedItems();
//        return (selectedItems != null) && (selectedItems.size() > 1);        
//    }

    public boolean isValidToDoItemEdit(Node itemToEdit, String category, String description, LocalDate startDate, LocalDate endDate, boolean completed) {
        return isValidNewToDoItem(category, description, startDate, endDate, completed);
    }

    public boolean isValidNewToDoItem(String category, String description, LocalDate startDate, LocalDate endDate, boolean completed) {
        if (category.trim().length() == 0)
            return false;
        if (description.trim().length() == 0)
            return false;
        if (startDate.isAfter(endDate))
            return false;
        return true;
    }

    public void addItem(Node itemToAdd) {
        ItemsController itemsController = new ItemsController((goLogoloApp)app);
        items.add(itemToAdd);
        
        if(itemToAdd instanceof DraggableRectangle){
             ((DraggableRectangle)itemToAdd).setOnMouseMoved(e->{
            //System.out.println("Entered");
            int pos = ((DraggableRectangle)itemToAdd).isXYonBoarder((int)e.getX(), (int)e.getY());
            if(pos==0){app.getGUIModule().getPrimaryScene().setCursor(Cursor.DEFAULT);}
            if(pos==1){app.getGUIModule().getPrimaryScene().setCursor(Cursor.NW_RESIZE);}
            if(pos==2){app.getGUIModule().getPrimaryScene().setCursor(Cursor.NE_RESIZE);}
            if(pos==3){app.getGUIModule().getPrimaryScene().setCursor(Cursor.SW_RESIZE);}
            if(pos==4){app.getGUIModule().getPrimaryScene().setCursor(Cursor.SE_RESIZE);}
            if(pos==5){app.getGUIModule().getPrimaryScene().setCursor(Cursor.N_RESIZE);}
            if(pos==6){app.getGUIModule().getPrimaryScene().setCursor(Cursor.S_RESIZE);}
            if(pos==7){app.getGUIModule().getPrimaryScene().setCursor(Cursor.W_RESIZE);}
            if(pos==8){app.getGUIModule().getPrimaryScene().setCursor(Cursor.E_RESIZE);}
//            else{app.getGUIModule().getPrimaryScene().setCursor(Cursor.E_RESIZE);}      
        });
            ((DraggableRectangle)itemToAdd).setOnMouseExited(e->{
            app.getGUIModule().getPrimaryScene().setCursor(Cursor.DEFAULT);    
        });

        }
        if(itemToAdd instanceof DraggableTriangle){     

            ((DraggableTriangle) itemToAdd).setOnMouseMoved(e->{
            //System.out.println("Entered");
            int pos = ((DraggableTriangle) itemToAdd).isXYonBoarder((int)e.getX(), (int)e.getY());
            if(pos==0){app.getGUIModule().getPrimaryScene().setCursor(Cursor.DEFAULT);}
            if(pos==1){app.getGUIModule().getPrimaryScene().setCursor(Cursor.HAND);}
            if(pos==2){app.getGUIModule().getPrimaryScene().setCursor(Cursor.HAND);}
            if(pos==3){app.getGUIModule().getPrimaryScene().setCursor(Cursor.HAND);}
//            else{app.getGUIModule().getPrimaryScene().setCursor(Cursor.E_RESIZE);}      
        });
            ((DraggableTriangle) itemToAdd).setOnMouseExited(e->{
            app.getGUIModule().getPrimaryScene().setCursor(Cursor.DEFAULT);    
        });
        }
        else if(itemToAdd instanceof DraggableText){

            
//            Font qwefont = ((DraggableText) itemToAdd).getFont();
//            String name = ((DraggableText) itemToAdd).getFont().getName();
//            String family = ((DraggableText) itemToAdd).getFont().getFamily();
//            String style = ((DraggableText) itemToAdd).getFont().getStyle();
//            double size = ((DraggableText) itemToAdd).getFont().getSize();
//            int xxx = 1;
                itemToAdd.setOnMouseClicked(e -> {
                if (e.getClickCount() == 2) {
                    String t1=AppDialogsFacade.showTextInputDialog(app.getGUIModule().getWindow(), APP_TEXTINPUT_TITLE, APP_TEXTINPUT_CONTENT);

                    if (!t1.equals("")) {
                        goLogoloData data = (goLogoloData)app.getDataComponent();
                        EditItem_Transaction transaction = new EditItem_Transaction(data,((DraggableText) itemToAdd), ((DraggableText) itemToAdd).getText(),t1);
                        app.processTransaction(transaction);
                        //text.setText(t1);
                    }
                }
            });
            
        }
        else if(itemToAdd instanceof DraggableCircle){
            itemToAdd.setOnScroll(e->{
                double deltaY = e.getDeltaY();
                double sizeFactor = 1.05;
                if (deltaY < 0){
                  sizeFactor = 2.0 - sizeFactor;
                }
                
                goLogoloData data = (goLogoloData)app.getDataComponent();
                SizeCircle_Transaction transaction = new SizeCircle_Transaction(data,((DraggableCircle) itemToAdd), ((DraggableCircle) itemToAdd).getRadius(),((DraggableCircle) itemToAdd).getRadius()*sizeFactor);
                app.processTransaction(transaction);
                e.consume();
            });       
        }
        else if(itemToAdd instanceof DraggableImage){
             

        }
        
        setOrder(items);
        itemsController.hilightAndUnhilightOthers(itemToAdd);

        //((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.setClip(itemToAdd);
    }

    public void removeItem(Node itemToRemove) {
        items.remove(itemToRemove);
        setOrder(items);
    }
    
    public void moveItem(Node itemToMove,double newX, double newY) {
        if(itemToMove instanceof DraggableCircle){
            ((DraggableCircle) itemToMove).setCenterX(newX);
            ((DraggableCircle) itemToMove).setCenterY(newY);
        }
        
        ((Draggable)itemToMove).setLocationAndSize(newX, newY, ((Draggable)itemToMove).getWidth(), ((Draggable)itemToMove).getHeight());
    }
    public void sizeRec(Node itemToMove,double newX, double newY,double newWidth,double newHeight) {
 
        ((Draggable)itemToMove).setLocationAndSize(newX, newY, newWidth,newHeight);
    }
     public void sizeCircle(DraggableCircle itemToMove,double newRadius){
         itemToMove.setRadius(newRadius);
     }

    public Node getSelectedItem() {
        return null;
    }


    public int getItemIndex(Node item) {
        return items.indexOf(item);
    }

    public void addItemAt(Node item, int itemIndex) {
        items.add(itemIndex, item);
    }

    public void moveItem(int oldIndex, int newIndex) {
        Node itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
    }

    public int getNumItems() {
        return items.size();
    }

    public void selectItem(Node itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }

    public ArrayList<Integer> removeAll(ArrayList<Node> itemsToRemove) {
        ArrayList<Integer> itemIndices = new ArrayList();
        for (Node item: itemsToRemove) {
            itemIndices.add(items.indexOf(item));
        }
        for (Node item: itemsToRemove) {
            items.remove(item);
        }
        return itemIndices;
    }

    public void addAll(ArrayList<Node> itemsToAdd, ArrayList<Integer> addItemLocations) {
        for (int i = 0; i < itemsToAdd.size(); i++) {
            Node itemToAdd = itemsToAdd.get(i);
            Integer location = addItemLocations.get(i);
            items.add(location, itemToAdd);
        }
    }

    public ArrayList<Node> getCurrentItemsOrder() {
        ArrayList<Node> orderedItems = new ArrayList();
        for (Node item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public void sortItems(Comparator sortComparator) {
        Collections.sort(items, sortComparator);
    }

    public void rearrangeItems(ArrayList<Node> oldListOrder) {
        items.clear();
        for (Node item : oldListOrder) {
            items.add(item);
        }
    }

    public void EditTextItem(DraggableText TextItem, String oldText, String newText) {
        TextItem.setText(newText);
    }
    
    public void setOrder(ObservableList<Node> items){
        for(Node temp:items){
            ((Draggable)temp).setOrder(Integer.toString(items.indexOf(temp)));
        }
    }

    public ObservableList<Node> getItems() {
        return items;
    }

    public void moveTriangle(Node itemToMove, ArrayList<Double> newpoints) {
        ((DraggableTriangle)itemToMove).getPoints().clear();
        ((DraggableTriangle)itemToMove).getPoints().addAll(newpoints);
    }
    
}