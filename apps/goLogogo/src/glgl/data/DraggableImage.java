package glgl.data;


import java.io.File;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class DraggableImage extends ImageView implements Draggable {
    double startX;
    double startY;
    final StringProperty order;
    final StringProperty name;
    final StringProperty type;
    public static final String DEFAULT_ORDER = "order";
    public static final String DEFAULT_NAME = "Image_name";
    public static final String DEFAULT_TYPE = "Image";
    
    public File resource;
    

    public DraggableImage() {
        setX(0.0);
        setY(0.0);
        setOpacity(1.0);
        startX = 0.0;
        startY = 0.0;
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    
    @Override
    public DraggableImage copyNode() {
        DraggableImage cloneImage = new DraggableImage();
        cloneImage.setImage(getImage());
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        cloneImage.setX(this.getX()+10);
        cloneImage.setY(this.getY()+10);
        cloneImage.setOpacity(getOpacity());
        return cloneImage;
    }
    
//    @Override
//    public golState getStartingState() {
//	return golState.STARTING_RECTANGLE;
//    }
    
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
	setX(x);
	setY(y);
    }
    
//    @Override
//    public void setStart(int initStartX, int initStartY) {
//        startX = initStartX;
//        startY = initStartY;
//    }
    
    @Override
    public void drag(int x, int y) {
	//double diffX = x - (getX() + (getWidth()/2));
	//double diffY = y - (getY() + (getHeight()/2));
        double diffX = x - (getX() + (getWidth() / 2));
        double diffY = y - (getY() + (getHeight() / 2));
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        setX(newX);
	setY(newY);
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	// WE DON'T RESIZE THE IMAGE	
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
	// WE DON'T RESIZE THE IMAGE
    }
    
//    @Override
//    public String getNodeType() {
//	return IMAGE;
//    }

    @Override
    public double getWidth() {
        return super.getImage().getWidth();
    }

    @Override
    public double getHeight() {
        return super.getImage().getHeight();
    }



    @Override
    public String getShapeType() {
        return IMAGE;    
    }


    public String getOrder() {
        return order.get();
    }
    public void setOrder(String value) {
        order.set(value);
    }
    public StringProperty orderProperty(){
        return order;
    }
        public String getName() {
        return name.get();
    }
    public void setName(String value) {
        name.set(value);
    }
    public StringProperty nameProperty(){
        return name;
    }
        public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }
    public StringProperty typeProperty(){
        return type;
    }

    @Override
    public int isXYonBoarder(int x, int y) {
        return 0;    
    }
    
}