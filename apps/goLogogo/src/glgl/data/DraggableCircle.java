package glgl.data;

import static glgl.data.Draggable.IMAGE;
import static glgl.data.DraggableRectangle.DEFAULT_FILL;
import static glgl.data.DraggableRectangle.DEFAULT_NAME;
import static glgl.data.DraggableRectangle.DEFAULT_ORDER;
import static glgl.data.DraggableRectangle.DEFAULT_TYPE;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import properties_manager.PropertiesManager;


/**
 * This is a draggable Circle for our goLogoLo application.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableCircle extends Circle implements Draggable {
    double startCenterX;
    double startCenterY;
    final StringProperty order;
    final StringProperty name;
    final StringProperty type;
    public static final String DEFAULT_ORDER = "order";
    public static final String DEFAULT_NAME = "Circle_name";
    public static final String DEFAULT_TYPE = "Circle";
    
    public DraggableCircle() {
	setCenterX(100);
	setCenterY(100);
	setRadius(100.0);
	setOpacity(1.0);
        setStroke(Color.YELLOW);
        setStrokeWidth(0);
        setFill(DEFAULT_FILL);
	startCenterX = 0.0;
	startCenterY = 0.0;
        order = new SimpleStringProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    
    @Override
    public DraggableCircle copyNode() {
        DraggableCircle cloneCircle = new DraggableCircle();
        cloneCircle.setRadius(getRadius());
        cloneCircle.setCenterX(this.getCenterX()+10);
        cloneCircle.setCenterY(this.getCenterY()+10);
        cloneCircle.setOpacity(getOpacity());
        cloneCircle.setFill(getFill());
        cloneCircle.setStroke(getStroke());
        cloneCircle.setStrokeWidth(getStrokeWidth());
        return cloneCircle;
    }
    
//    @Override
//    public golState getStartingState() {
//	return golState.STARTING_Circle;
//    }
    
    @Override
    public void start(int x, int y) {
	startCenterX = x;
	startCenterY = y;
    }
    
//    @Override
//    public void setStart(int initStartX, int initStartY) {
//        startCenterX = initStartX;
//        startCenterY = initStartY;
//    }
    
    @Override
    public void drag(int x, int y) {
//	double diffX = x - startCenterX;
//	double diffY = y - startCenterY;
//	double newX = getCenterX() + diffX;
//	double newY = getCenterY() + diffY;
	setCenterX(x);
	setCenterY(y);
	startCenterX = x;
	startCenterY = y;
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadius(width / 2);
	
    }
        
    @Override
    public double getX() {
	return getCenterX() - getRadius();
    }

    @Override
    public double getY() {
	return getCenterY() - getRadius();
    }

    @Override
    public double getWidth() {
	return getRadius() * 2;
    }

    @Override
    public double getHeight() {
	return getRadius() * 2;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadius(initWidth/2);
	setRadius(initHeight/2);
    }
        @Override
    public String getShapeType() {
        return IMAGE;    
    }


    public String getOrder() {
        return order.get();
    }
    public void setOrder(String value) {
        order.set(value);
    }
    public StringProperty orderProperty(){
        return order;
    }
        public String getName() {
        return name.get();
    }
    public void setName(String value) {
        name.set(value);
    }
    public StringProperty nameProperty(){
        return name;
    }
        public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }
    public StringProperty typeProperty(){
        return type;
    }

    @Override
    public int isXYonBoarder(int x, int y) {
        return 0;    
    }
    
//    @Override
//    public String getNodeType() {
//	return Circle;
//    }
}
