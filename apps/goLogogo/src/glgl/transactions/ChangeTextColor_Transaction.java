/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

public class ChangeTextColor_Transaction implements jTPS_Transaction {
    private Text text;
    private Color color;
    private Color oldcolor;

    public ChangeTextColor_Transaction(Text initText, Color initColor) {
        text = initText;
        color = initColor;
        oldcolor = (Color)text.getFill();
    }
    
 
    @Override
    public void doTransaction() {
        text.setFill(color);
    }

    @Override
    public void undoTransaction() {
        text.setFill(oldcolor);
    }    
}