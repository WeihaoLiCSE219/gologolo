/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.Draggable;
import glgl.data.DraggableRectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

public class BorderRadius_Transaction implements jTPS_Transaction {
    private DraggableRectangle node;
    private double oldBorderRadius;
    private double newBorderRadius;
    
    public BorderRadius_Transaction(Draggable shape, double oldBorderRadius1, double newBorderRadius1) {
        node = (DraggableRectangle)shape;
        oldBorderRadius = oldBorderRadius1;
        newBorderRadius = newBorderRadius1;
    }
    
 
    @Override
    public void doTransaction() {
        node.setArcWidth(newBorderRadius);
        node.setArcHeight(newBorderRadius);
    }

    @Override
    public void undoTransaction() {
        node.setArcWidth(oldBorderRadius);
        node.setArcHeight(oldBorderRadius);
    }    
}