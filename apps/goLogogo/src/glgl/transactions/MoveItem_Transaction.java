package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class MoveItem_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Node itemToMove;
    double oldX;
    double oldY;
    double newX;
    double newY;

    public MoveItem_Transaction(goLogoloData data, Node itemToMove, double oldX, double oldY, double newX, double newY) {
        this.data = data;
        this.itemToMove = itemToMove;
        this.oldX = oldX;
        this.oldY = oldY;
        this.newX = newX;
        this.newY = newY;
    }
    
    

    @Override
    public void doTransaction() {
        data.moveItem(itemToMove,newX,newY);        
    }

    @Override
    public void undoTransaction() {
        data.moveItem(itemToMove,oldX,oldY);
    }
}
