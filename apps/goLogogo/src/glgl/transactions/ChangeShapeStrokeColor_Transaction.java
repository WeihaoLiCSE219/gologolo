/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.Draggable;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

public class ChangeShapeStrokeColor_Transaction implements jTPS_Transaction {
    private Shape node;
    private Color oldColor;
    private Color newColor;
    
    public ChangeShapeStrokeColor_Transaction(Draggable shape, Color oldColor1, Color newColor1) {
        node = (Shape)shape;
        oldColor = oldColor1;
        newColor = newColor1;

    }
    
 
    @Override
    public void doTransaction() {
        node.setStroke(newColor);
    }

    @Override
    public void undoTransaction() {
        node.setStroke(oldColor);
    }    
}