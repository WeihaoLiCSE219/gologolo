package glgl.transactions;

import jtps.jTPS_Transaction;
import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import glgl.goLogoloApp;
import glgl.data.goLogoloData;
import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class CutItems_Transaction implements jTPS_Transaction {
    goLogoloApp app;
    Node itemToCut;
    //ArrayList<Integer> cutItemLocations;
    
    public CutItems_Transaction(goLogoloApp initApp, Node initItemToCut) {
        app = initApp;
        itemToCut = initItemToCut;
    }

    @Override
    public void doTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        data.removeItem(itemToCut);
        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        data.addItem(itemToCut);
        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }   
}