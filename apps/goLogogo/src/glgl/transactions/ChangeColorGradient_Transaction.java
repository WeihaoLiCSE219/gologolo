/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.Draggable;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

public class ChangeColorGradient_Transaction implements jTPS_Transaction {
    private Shape node;
    private RadialGradient oldRadialGradient;
    private RadialGradient newRadialGradient;

    public ChangeColorGradient_Transaction(Shape node, RadialGradient oldRadialGradient, RadialGradient newRadialGradient) {
        this.node = node;
        this.oldRadialGradient = oldRadialGradient;
        this.newRadialGradient = newRadialGradient;
    }
    

    
 
    @Override
    public void doTransaction() {
        node.setFill(newRadialGradient);
    }

    @Override
    public void undoTransaction() {
        node.setFill(oldRadialGradient);
    }    
}