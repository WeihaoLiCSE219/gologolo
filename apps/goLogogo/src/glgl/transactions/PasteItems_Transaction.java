package glgl.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import glgl.goLogoloApp;
import glgl.data.goLogoloData;
import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class PasteItems_Transaction implements jTPS_Transaction {
    goLogoloApp app;
    Node itemToPaste;
    int pasteIndex;
    
    public PasteItems_Transaction(  goLogoloApp initApp, 
                                    Node initItemsToPaste) {
        app = initApp;
        itemToPaste = initItemsToPaste;
        //pasteIndex = initPasteIndex;
    }

    @Override
    public void doTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();

            data.addItem(itemToPaste);

        
    }

    @Override
    public void undoTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();

            data.removeItem(itemToPaste);

    }   
}