package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class SizeRectangle_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Node itemToMove;
    double oldX;
    double oldY;
    double newX;
    double newY;
    double oldWidth;
    double oldHeight;
    double newWidth;
    double newHeight;

    public SizeRectangle_Transaction(goLogoloData data, Node itemToMove, double oldX, double oldY, double newX, double newY, double oldWidth, double oldHeight, double newWidth, double newHeight) {
        this.data = data;
        this.itemToMove = itemToMove;
        this.oldX = oldX;
        this.oldY = oldY;
        this.newX = newX;
        this.newY = newY;
        this.oldWidth = oldWidth;
        this.oldHeight = oldHeight;
        this.newWidth = newWidth;
        this.newHeight = newHeight;
    }


    
    

    @Override
    public void doTransaction() {
        data.sizeRec(itemToMove,newX,newY,newWidth,newHeight);        
    }

    @Override
    public void undoTransaction() {
        data.sizeRec(itemToMove,oldX,oldY,oldWidth,oldHeight);
    }
}
