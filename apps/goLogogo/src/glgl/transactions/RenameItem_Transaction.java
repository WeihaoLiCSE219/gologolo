package glgl.transactions;

import glgl.data.Draggable;
import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class RenameItem_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Draggable itemToRename;
    String oldname;
    String newname;

    public RenameItem_Transaction(goLogoloData data, Draggable itemToMove, String oldname, String newname) {
        this.data = data;
        this.itemToRename = itemToMove;
        this.oldname = oldname;
        this.newname = newname;
    }

    @Override
    public void doTransaction() {
        itemToRename.setName(newname);        
    }

    @Override
    public void undoTransaction() {
        itemToRename.setName(oldname);        
    }
}
