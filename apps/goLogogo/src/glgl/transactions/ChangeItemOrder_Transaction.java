package glgl.transactions;

import glgl.data.Draggable;
import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;
import javafx.collections.ObservableList;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class ChangeItemOrder_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Draggable itemToRename;
    ObservableList<Node> Nodes;
    int sel;
    int op;

    public ChangeItemOrder_Transaction(goLogoloData data, Draggable itemToRename, ObservableList<Node> Nodes, int index,int op) {
        this.data = data;
        this.itemToRename = itemToRename;
        this.Nodes = Nodes;
        this.sel = index;
        this.op = op;
    }



    @Override
    public void doTransaction() {
        if(op==1){
            Nodes.add(sel, Nodes.remove(sel-1));
            sel--;
            data.setOrder(Nodes);          
        }
        if(op==2){
            Nodes.add(sel, Nodes.remove(sel+1));
            sel++;
            data.setOrder(Nodes);           
        }

    }

    @Override
    public void undoTransaction() {
        if(op==1){
            Nodes.add(sel, Nodes.remove(sel+1));
            sel++;
            data.setOrder(Nodes);                
        }
        if(op==2){
            Nodes.add(sel, Nodes.remove(sel-1));
            sel--;
            data.setOrder(Nodes);           
        }
 
    }
}
