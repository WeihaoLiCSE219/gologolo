package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;
import java.util.ArrayList;
import javafx.collections.ObservableList;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class MoveTriangle_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Node itemToMove;
    ArrayList<Double> oldpoints;
    ArrayList<Double> newpoints;

    public MoveTriangle_Transaction(goLogoloData data, Node itemToMove, ArrayList<Double> oldpoints, ArrayList<Double> newpoints) {
        this.data = data;
        this.itemToMove = itemToMove;
        this.oldpoints = oldpoints;
        this.newpoints = newpoints;
    }


    
    

    @Override
    public void doTransaction() {
        data.moveTriangle(itemToMove,newpoints);        
    }

    @Override
    public void undoTransaction() {
        data.moveTriangle(itemToMove,oldpoints);        
    }
}
