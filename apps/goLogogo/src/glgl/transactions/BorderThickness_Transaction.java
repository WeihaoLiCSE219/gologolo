/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.Draggable;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

public class BorderThickness_Transaction implements jTPS_Transaction {
    private Shape node;
    private double oldBorderThickness;
    private double newBorderThickness;
    
    public BorderThickness_Transaction(Draggable shape, double oldBorderThickness1, double newBorderThickness1) {
        node = (Shape)shape;
        oldBorderThickness = oldBorderThickness1;
        newBorderThickness = newBorderThickness1;
    }
    
 
    @Override
    public void doTransaction() {
        node.setStrokeWidth(newBorderThickness);
    }

    @Override
    public void undoTransaction() {
        node.setStrokeWidth(oldBorderThickness);
    }    
}