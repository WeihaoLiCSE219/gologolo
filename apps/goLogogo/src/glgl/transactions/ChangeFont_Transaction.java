/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

public class ChangeFont_Transaction implements jTPS_Transaction {
    private Text text;
    private Font font;
    private Font oldFont;
    
    public ChangeFont_Transaction(Text initText, Font initFont) {
        text = initText;
        font = initFont;
        oldFont = text.getFont();
    }
 
    @Override
    public void doTransaction() {
        text.setFont(font);
    }

    @Override
    public void undoTransaction() {
        text.setFont(oldFont);
    }    
}