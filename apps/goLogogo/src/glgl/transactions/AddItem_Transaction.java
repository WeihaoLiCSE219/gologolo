package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class AddItem_Transaction implements jTPS_Transaction {
    goLogoloData data;
    Node itemToAdd;
    
    public AddItem_Transaction(goLogoloData initData, Node initNewItem) {
        data = initData;
        itemToAdd = initNewItem;
    }

    @Override
    public void doTransaction() {
        data.addItem(itemToAdd);        
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
    }
}
