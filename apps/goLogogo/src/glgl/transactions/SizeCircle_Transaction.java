package glgl.transactions;

import glgl.data.DraggableCircle;
import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class SizeCircle_Transaction implements jTPS_Transaction {
    goLogoloData data;
    DraggableCircle itemToMove;
    double oldRadius;
    double newRadius;

    public SizeCircle_Transaction(goLogoloData data, DraggableCircle itemToMove, double oldRadius, double newRadius) {
        this.data = data;
        this.itemToMove = itemToMove;
        this.oldRadius = oldRadius;
        this.newRadius = newRadius;
    }






    
    

    @Override
    public void doTransaction() {
        data.sizeCircle(itemToMove,newRadius);    
    }

    @Override
    public void undoTransaction() {
        data.sizeCircle(itemToMove,oldRadius);    
    }
}
