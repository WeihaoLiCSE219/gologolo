package glgl.transactions;

import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import java.util.ArrayList;
import jtps.jTPS_Transaction;
import glgl.goLogoloApp;
import glgl.data.goLogoloData;
import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class RemoveItems_Transaction implements jTPS_Transaction {
    goLogoloApp app;
    Node itemToRemove;
    ArrayList<Integer> removedItemLocations;
    
    public RemoveItems_Transaction(goLogoloApp initApp, Node initItems) {
        app = initApp;
        itemToRemove = initItems;
    }

    @Override
    public void doTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        data.removeItem(itemToRemove);
    }

    @Override
    public void undoTransaction() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        data.addItem(itemToRemove);
    }
}