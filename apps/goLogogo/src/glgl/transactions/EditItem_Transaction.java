package glgl.transactions;

import glgl.data.DraggableText;
import jtps.jTPS_Transaction;
import glgl.data.goLogoloData;

import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class EditItem_Transaction implements jTPS_Transaction {
    goLogoloData data;
    DraggableText itemToAdd;
    String oldText;
    String newText;

    public EditItem_Transaction(goLogoloData data, DraggableText itemToAdd, String oldText, String newText) {
        this.data = data;
        this.itemToAdd = itemToAdd;
        this.oldText = oldText;
        this.newText = newText;
    }
    


    @Override
    public void doTransaction() {
        data.EditTextItem(itemToAdd,oldText,newText);        
    }

    @Override
    public void undoTransaction() {
        data.EditTextItem(itemToAdd,newText,oldText);
    }
}
