package glgl.workspace.controllers;

import static djf.AppPropertyType.*;
import djf.ui.dialogs.AppDialogsFacade;
import java.util.ArrayList;
import glgl.goLogoloApp;
import glgl.data.goLogoloData;

import glgl.workspace.dialogs.goLogoloLDialog;
import glgl.workspace.goLogoloWorkspace;
import glgl.data.Draggable;
import glgl.data.DraggableCircle;
import glgl.data.DraggableImage;
import glgl.data.DraggableRectangle;
import glgl.data.DraggableText;
import glgl.data.DraggableTriangle;
import static glgl.goLogoloPropertyType.GLGL_TABLEVIEW;
import glgl.transactions.*;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.TableView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.Pair;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    goLogoloApp app;
    goLogoloLDialog itemDialog;
    
    double oldX;
    double oldY;
    double oldWidth;
    double oldHeight;
    
    ArrayList<Double> oldPoints;
    
    int CurrentOP=0;
    int PosOnRecNow=0;
    int PosOnTriNow=0;
    
    String oldFont;
    double oldFontSize;
    
    
    public static double STROKE_WIDTH = 0;
    
    
    
    
    public ItemsController(goLogoloApp initApp) {
        app = initApp;
        
        itemDialog = new goLogoloLDialog(app);
        

    }
    
//    public void processAddItem() {
//        itemDialog.showAddDialog();
//        NodePrototype newItem = itemDialog.getNewItem();        
//        if (newItem != null) {
//            // IF IT HAS A UNIQUE NAME AND COLOR
//            // THEN CREATE A TRANSACTION FOR IT
//            // AND ADD IT
//            goLogoloData data = (goLogoloData)app.getDataComponent();
//            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
//            app.processTransaction(transaction);
//        }    
//        // OTHERWISE TELL THE USER WHAT THEY
//        // HAVE DONE WRONG
//        else {
//            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
//        }
//    }
    
//    public void processRemoveItems() {
//        goLogoloData data = (goLogoloData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            ArrayList<goLogoloItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
//            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
//            app.processTransaction(transaction);
//        }
//    }

//    public void processEditItem() {
//        goLogoloData data = (goLogoloData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            NodePrototype itemToEdit = data.getSelectedItem();
//            itemDialog.showEditDialog(itemToEdit);
//            NodePrototype editItem = itemDialog.getEditItem();
//            if (editItem != null) {
//                EditItem_Transaction transaction = new EditItem_Transaction(itemToEdit, editItem.getCategory(), editItem.getDescription(), editItem.getStartDate(), editItem.getEndDate(), editItem.isCompleted());
//                app.processTransaction(transaction);
//            }
//        }
//    }
    
//    public void processMoveItemUp() {
//        goLogoloData data = (goLogoloData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            NodePrototype itemToMove = data.getSelectedItem();
//            int oldIndex = data.getItemIndex(itemToMove);
//            if (oldIndex > 0) {
//                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex-1);
//                app.processTransaction(transaction);
//                
//                // DESELECT THE OLD INDEX
//                data.clearSelected();
//                
//                // AND SELECT THE MOVED ONE
//                data.selectItem(itemToMove);
//                
//                // UPDATE BUTTONS
//                app.getFoolproofModule().updateAll();
//            }
//        }
//    }
//    
//    public void processMoveItemDown() {
//        goLogoloData data = (goLogoloData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            NodePrototype itemToMove = data.getSelectedItem();
//            int oldIndex = data.getItemIndex(itemToMove);
//            if (oldIndex < (data.getNumItems()-1)) {
//                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex+1);
//                app.processTransaction(transaction);
//                
//                // DESELECT THE OLD INDEX
//                data.clearSelected();
//                
//                // AND SELECT THE MOVED ONE
//                data.selectItem(itemToMove);
//                
//                // UPDATE BUTTONS
//                app.getFoolproofModule().updateAll();
//            }
//        }
//    }
//
//    public void processLargerItem() {
//        itemDialog.showChangeDimensionDialog();
//    }

    public void processAddNode(String type) {
        //System.out.println("123");
        if(type.equals("Rect")){
            DraggableRectangle x = new DraggableRectangle(100.0,100.0,200.0,200.0,"1","2","3");
            x.setStroke(Color.YELLOW);
            x.setStrokeWidth(0);
            x.setName("name");
            x.setType("Rect");
            
            goLogoloData data = (goLogoloData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, x);
            app.processTransaction(transaction);
            
            //((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren().add(x);
            //ObservableList<Node> Shapes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        }
        else if(type.equals("Text")){
            String result=AppDialogsFacade.showTextInputDialog(app.getGUIModule().getWindow(), APP_TEXTINPUT_TITLE, APP_TEXTINPUT_CONTENT);
            if (!result.equals("")) {
                DraggableText text = new DraggableText();
                text.setText(result);
                text.setX(100);
                text.setY(100);
                text.setFont(Font.font("Arial",FontWeight.BOLD,FontPosture.REGULAR,80.0));
                text.setStroke(Color.YELLOW);
                text.setStrokeWidth(0);
                text.setName("name");
                text.setType("Text");
            //hilightAndUnhilightOthers(text);
            //((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren().add(text);

//            dataManager.addShape(text);
            

            
            
            goLogoloData data = (goLogoloData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, text);
            app.processTransaction(transaction);
        }
        }
        else if(type.equals("Image")){
            Image choseImage = AppDialogsFacade.showChooseImageDialog();
            if(choseImage == null){
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), IMAGE_ERROR_TITLE, IMAGE_ERROR_CONTENT);
            }
            else{
                DraggableImage imageViewToAdd = new DraggableImage();
                imageViewToAdd.resource = AppDialogsFacade.imageFile;
                imageViewToAdd.setImage(choseImage);
                imageViewToAdd.setName("name");
                imageViewToAdd.setType("Image");
                PropertiesManager props = PropertiesManager.getPropertiesManager();

                // MAKE AND ADD THE TRANSACTION
                goLogoloData data = (goLogoloData)app.getDataComponent();
                AddItem_Transaction transaction = new AddItem_Transaction(data, imageViewToAdd);
                app.processTransaction(transaction);
            }
            
        }
        else if(type.equals("Circle")){
            DraggableCircle x = new DraggableCircle();
            x.setName("name");
            x.setType("Circle");
            goLogoloData data = (goLogoloData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, x);
            app.processTransaction(transaction);
            
        }
        else if(type.equals("Tri")){
            DraggableTriangle x = new DraggableTriangle();
            x.setName("name");
            x.setType("Triangle");           
            goLogoloData data = (goLogoloData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, x);
            app.processTransaction(transaction);
            
        }        
        
        app.getFoolproofModule().updateAll();

        //app.getWorkspaceComponent().getWorkspace().getChildren().add(x);
    }

    public void processWorkspacePaneMousePress(int x, int y) {
        boolean isSelected = false;
        ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        for(int i = Nodes.size()-1; i>=0;i--){
            if(Nodes.get(i).contains(x, y)){
                hilightAndUnhilightOthers((Node)(Draggable)Nodes.get(i));
                isSelected = true;
                if(Nodes.get(i) instanceof DraggableRectangle){
                    oldX=((Draggable)Nodes.get(i)).getX();
                    oldY=((Draggable)Nodes.get(i)).getY();
                    oldWidth = ((Draggable)Nodes.get(i)).getWidth();
                    oldHeight = ((Draggable)Nodes.get(i)).getHeight();
                    goLogoloData data = (goLogoloData)app.getDataComponent();
                    int posOnRec = ((Draggable)data.selectedDraggableShape).isXYonBoarder(x,y);
                    if(posOnRec!=0){CurrentOP=2;PosOnRecNow = posOnRec;}
                    else{CurrentOP=1;PosOnRecNow = 0;}
                    break;
                }
                if(Nodes.get(i) instanceof DraggableText){
                    oldX=((Draggable)Nodes.get(i)).getX();
                    oldY=((Draggable)Nodes.get(i)).getY();
//                    oldWidth = ((Draggable)Nodes.get(i)).getWidth();
//                    oldHeight = ((Draggable)Nodes.get(i)).getHeight();
                    goLogoloData data = (goLogoloData)app.getDataComponent();
                    //int posOnRec = ((Draggable)data.selectedDraggableShape).isXYonBoarder(x,y);
                    CurrentOP=1;
                    break;
                }
                if(Nodes.get(i) instanceof DraggableImage){
                    oldX=((Draggable)Nodes.get(i)).getX();
                    oldY=((Draggable)Nodes.get(i)).getY();
//                    oldWidth = ((Draggable)Nodes.get(i)).getWidth();
//                    oldHeight = ((Draggable)Nodes.get(i)).getHeight();
                    goLogoloData data = (goLogoloData)app.getDataComponent();
                    //int posOnRec = ((Draggable)data.selectedDraggableShape).isXYonBoarder(x,y);
                    CurrentOP=1;
                    break;
                }
                else if(Nodes.get(i) instanceof DraggableTriangle){
                    oldX=((DraggableTriangle)Nodes.get(i)).getPoints().get(0);
                    oldY=((DraggableTriangle)Nodes.get(i)).getPoints().get(1);
                    oldPoints = ((DraggableTriangle)Nodes.get(i)).getAllPoints();
                    goLogoloData data = (goLogoloData)app.getDataComponent();
                    int posOnTri = ((Draggable)data.selectedDraggableShape).isXYonBoarder(x,y);
                    if(posOnTri!=0){CurrentOP=2;PosOnTriNow = posOnTri;}
                    else{CurrentOP=1;PosOnTriNow = 0;}
                    break;
                }
                else if(Nodes.get(i) instanceof DraggableCircle){
                    oldX=((DraggableCircle)Nodes.get(i)).getX();
                    oldY=((DraggableCircle)Nodes.get(i)).getY();
//                    oldWidth = ((Draggable)Nodes.get(i)).getWidth();
//                    oldHeight = ((Draggable)Nodes.get(i)).getHeight();
                    goLogoloData data = (goLogoloData)app.getDataComponent();
                    //int posOnRec = ((Draggable)data.selectedDraggableShape).isXYonBoarder(x,y);
                    CurrentOP=1;
                    break;
                }
                
                //System.out.println("123");
                //((DraggableRectangle)selectedDraggableShape).setStroke(Color.YELLOW);
            }
        } 
        if(!isSelected){
            hilightAndUnhilightOthers(null);
        }
//        selectedDraggableShape=null;
        app.getFoolproofModule().updateAll();
    }
    public void processWorkspacePaneMouseDragged(int x, int y) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null){
            //int posOnRec = ((DraggableRectangle)data.selectedDraggableShape).isXYonBoarder(x,y);
            if(CurrentOP==2){
                if(data.selectedDraggableShape instanceof DraggableRectangle){((DraggableRectangle)data.selectedDraggableShape).sizeDepOnPos(x, y,PosOnRecNow);}
                else if(data.selectedDraggableShape instanceof DraggableTriangle){((DraggableTriangle)data.selectedDraggableShape).sizeDepOnPos(x, y,PosOnTriNow);}

                
            }
//            if(data.selectedDraggableShape instanceof DraggableRectangle && ((DraggableRectangle)data.selectedDraggableShape).isXYonBoarder(x,y)!=0){
//                CurrentOP = 2;
//                if(CurrentOP==2){
//                    ((DraggableRectangle)data.selectedDraggableShape).sizeDepOnPos(x, y,((DraggableRectangle)data.selectedDraggableShape).isXYonBoarder(x,y));
//                }
//            }
            else if(CurrentOP==1){
                Draggable sss = ((Draggable)data.selectedDraggableShape);
                sss.drag(x, y);
            }
            
        }
        app.getFoolproofModule().updateAll();
    }

    public void processWorkspacePaneMouseRelease(int x, int y) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null){
            Node oo=(Node) data.selectedDraggableShape;
            if(data.selectedDraggableShape instanceof DraggableTriangle && CurrentOP == 1 &&(   ((DraggableTriangle)data.selectedDraggableShape).getPoints().get(0)!=oldX ||((DraggableTriangle)data.selectedDraggableShape).getPoints().get(1)!=oldY      )){
                MoveTriangle_Transaction transaction = new MoveTriangle_Transaction(data, oo,oldPoints,((DraggableTriangle)data.selectedDraggableShape).getAllPoints());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();  
            }
             if(data.selectedDraggableShape instanceof DraggableText && CurrentOP == 1 &&(    ((Draggable)data.selectedDraggableShape).getX()!=oldX  ||  ((Draggable)data.selectedDraggableShape).getY()!=oldY   )){
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oo,oldX,oldY,data.selectedDraggableShape.getX(),data.selectedDraggableShape.getY());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();
            }         
            if(data.selectedDraggableShape instanceof DraggableImage && CurrentOP == 1 &&(    ((Draggable)data.selectedDraggableShape).getX()!=oldX  ||  ((Draggable)data.selectedDraggableShape).getY()!=oldY   )){
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oo,oldX,oldY,data.selectedDraggableShape.getX(),data.selectedDraggableShape.getY());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();
            }    
             if(data.selectedDraggableShape instanceof DraggableCircle && CurrentOP == 1 &&(    ((Draggable)data.selectedDraggableShape).getX()!=oldX  ||  ((Draggable)data.selectedDraggableShape).getY()!=oldY   )){
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oo,oldX,oldY,data.selectedDraggableShape.getX(),data.selectedDraggableShape.getY());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();
            }   

            else if(data.selectedDraggableShape instanceof DraggableRectangle && CurrentOP == 1 && (oldX!=data.selectedDraggableShape.getX() || oldY!=data.selectedDraggableShape.getY())){
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oo,oldX,oldY,data.selectedDraggableShape.getX(),data.selectedDraggableShape.getY());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();
//        ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
//        for(int i = Nodes.size()-1; i>=0;i--){
//            if(Nodes.get(i).contains(x, y)){
//                hilightAndUnhilightOthers((Node)(Draggable)Nodes.get(i)) ;
//                break;
//                //System.out.println("123");
//                //((DraggableRectangle)selectedDraggableShape).setStroke(Color.YELLOW);
//            }
//        } 
                //app.getFoolproofModule().updateAll();
            }
            else if(CurrentOP == 2){
                SizeRectangle_Transaction transaction = new SizeRectangle_Transaction(data, oo,oldX,oldY,data.selectedDraggableShape.getX(),data.selectedDraggableShape.getY(),oldWidth,oldHeight,data.selectedDraggableShape.getWidth(),data.selectedDraggableShape.getHeight());
                app.processTransaction(transaction);
                app.getFoolproofModule().updateAll();
            }

        }

    }

//    public void processWorkspacePaneMouseClicked(int x, int y) {
//        ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
//        for(int i = Nodes.size()-1; i>=0;i--){
//            if(Nodes.get(i).contains(x, y)){
//                selectedDraggableShape = (Draggable)Nodes.get(i);
//                //System.out.println("123");
//                ((Shape)selectedDraggableShape).setStroke(Color.YELLOW);
//            }
//        } 
//    }

    public void processRemoveItem() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        //ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        if((Node)data.selectedDraggableShape!=null){
            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, (Node)data.selectedDraggableShape);
            app.processTransaction(transaction);
            app.getFoolproofModule().updateAll();
        }

    }
    public void hilightAndUnhilightOthers(Node T){
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(T!=null){
            if(data.selectedDraggableShape!=null){
                if(data.selectedDraggableShape instanceof DraggableImage){
                    ((DraggableImage)data.selectedDraggableShape).setEffect(null);
                    ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(null);

                }
                else{                
                  ((Shape)data.selectedDraggableShape).setEffect(null); 
                  ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(null);
                }
//                ((Shape)data.selectedDraggableShape).setStrokeWidth(0);
//                ((Shape)data.selectedDraggableShape).setStroke(null);
            }
            data.selectedDraggableShape = (Draggable) T;
            if(data.selectedDraggableShape instanceof DraggableImage){
                ((DraggableImage)data.selectedDraggableShape).setEffect(new DropShadow(10, Color.BLACK));
                ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(data.selectedDraggableShape);

                
            }
            else{
                ((Shape)data.selectedDraggableShape).setEffect(new DropShadow(10, Color.BLACK));
                ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(data.selectedDraggableShape);

            }
//            ((Shape)data.selectedDraggableShape).setStrokeWidth(STROKE_WIDTH);
//            ((Shape)data.selectedDraggableShape).setStroke(Color.YELLOW);            
        }
        else{
            if(data.selectedDraggableShape!=null){
                if(data.selectedDraggableShape instanceof DraggableImage){
                    ((DraggableImage)data.selectedDraggableShape).setEffect(null);
                    ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(null);
                }
                else{                
                  ((Shape)data.selectedDraggableShape).setEffect(null); 
                  ((TableView) app.getGUIModule().getGUINode(GLGL_TABLEVIEW)).getSelectionModel().select(null);
                }
//                ((Shape)data.selectedDraggableShape).setStrokeWidth(0);
//                ((Shape)data.selectedDraggableShape).setStroke(null);
                data.selectedDraggableShape=null;               
            }

        }

    }

    public void processSelectItem(Node x) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
//        data.selectedDraggableShape = (Draggable)x;
        hilightAndUnhilightOthers(x);
    }

    public void processExport() {
        //AppWorkspaceComponent ws = app.getWorkspaceComponent();
        //AppFileModule fileSettings = app.getFileModule();
        Pane canvas = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane;
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(((Shape)data.selectedDraggableShape)!=null){
            ((Shape)data.selectedDraggableShape).setEffect(null);
        }
        
//        ((Shape)data.selectedDraggableShape).setStrokeWidth(0);
//        ((Shape)data.selectedDraggableShape).setStroke(null);
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        if(((Shape)data.selectedDraggableShape)!=null){
            ((Shape)data.selectedDraggableShape).setEffect(new DropShadow(10, Color.BLACK));
        }
        

        File exportFile = AppDialogsFacade.showExportDialog(app.getGUIModule().getWindow(), EXPORT_WORK_TITLE);
        if (exportFile != null) {
            String Path = exportFile.getPath();
            if(Path.contains(".png")){
                Path = Path.replace(".png", "");
            }
            //String name = exportFile.getName();
            File file = new File(Path +".png");
//            String name = "logo";
//            File file = new File("work/" + name + ".png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), EXPORT_WORK_TITLE, EXPORT_SUCCESS_TITLE);
 
        } catch (IOException ioe) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), EXPORT_ERROR_TITLE, EXPORT_ERROR_CONTENT);
            ioe.printStackTrace();
        }
        }    
    }

    public void processResetSize() {
        Pane canvas = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane;
        canvas.setScaleX(1);
        canvas.setScaleY(1);    }
    public void processbigger() {
        Pane canvas = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane;
        double x= canvas.getScaleX()*1.1;
        double y= canvas.getScaleY()*1.1;
        if(x>1.0 || y>1.0){x=1;y=1;}
        canvas.setScaleX(x);
        canvas.setScaleY(y);
    }
    public void processsmaller() {
        Pane canvas = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane;
        double sizex= canvas.getHeight();
        double sizey= canvas.getWidth();
        canvas.setScaleX(canvas.getScaleX()*0.9);
        canvas.setScaleY(canvas.getScaleY()*0.9);    }

    public void processlarger() {
        Pane canvas = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane;
        Optional<Pair<String, String>> result=AppDialogsFacade.showChangeDimensionDialog("Change dimension dialog","The max width is "+canvas.getWidth()+".\nThe max height is "+canvas.getHeight()+".",canvas.getWidth()*canvas.getScaleX(),canvas.getHeight()*canvas.getScaleY());
        result.ifPresent(widthHeight -> {
            try{
                double width =  Double.parseDouble(widthHeight.getKey());
                double height=  Double.parseDouble(widthHeight.getValue());
//                double currentWidth  = canvas.getWidth()*canvas.getScaleX();
//                double currentHeight = canvas.getHeight()*canvas.getScaleY();
                double x =  width/canvas.getWidth();
                double y = height/canvas.getHeight();
                if(x>1.0 || y>1.0){x=1;y=1;}
                canvas.setScaleX(x);
                canvas.setScaleY(y);
                if(width>canvas.getWidth() || height>canvas.getHeight()){
                    throw new NumberFormatException();
                }
            }catch (NumberFormatException ioe) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), CHANGEDIMENSION_ERROR_TITLE, CHANGEDIMENSION_ERROR_CONTENT);
            //ioe.printStackTrace();
        }

        });
//        Object xx = result.get();
//        String yy = result.get().getKey();
//        result.ifPresent(usernamePassword -> {
//            System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());
//        });
//        int x=1;
    }

    public void processChangeFont(String fontname) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            Font loadedFont = Font.font(fontname, FontWeight.findByName(tochange.getFont().getFamily()), FontPosture.findByName(tochange.getFont().getStyle()), tochange.getFont().getSize());


            ChangeFont_Transaction transaction = new ChangeFont_Transaction(tochange, loadedFont);
            app.processTransaction(transaction);
        }
    }
    public void processChangeFontsize(String fontsize) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            String fontpostureString = tochange.getFont().getStyle();
            FontWeight fontweight;
            FontPosture fontposture;
            if(fontpostureString.contains("Bold")){
                fontweight = FontWeight.BOLD;
            }
            else{
                fontweight = FontWeight.NORMAL;
            }
            if(fontpostureString.contains("Italic")){
                fontposture = FontPosture.ITALIC;
            }
            else{
                fontposture = FontPosture.REGULAR;
            }
            Font loadedFont = Font.font(tochange.getFont().getFamily(), fontweight, fontposture, Double.parseDouble(fontsize));

            ChangeFont_Transaction transaction = new ChangeFont_Transaction(tochange, loadedFont);
            app.processTransaction(transaction);
        }
    }
    public void processChangeFontWeight() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            String fontweightString = tochange.getFont().getStyle().toString();
            FontWeight fontweight;
            FontPosture fontposture;
            if(fontweightString.contains("Bold")){
                fontweight = FontWeight.NORMAL;
            }
            else{
                fontweight = FontWeight.BOLD;
            }
            if(fontweightString.contains("Italic")){
                fontposture = FontPosture.ITALIC;
            }
            else{
                fontposture = FontPosture.REGULAR;
            }
            
            Font loadedFont = Font.font(tochange.getFont().getFamily(), fontweight, fontposture, tochange.getFont().getSize());

            ChangeFont_Transaction transaction = new ChangeFont_Transaction(tochange, loadedFont);
            app.processTransaction(transaction);
        }
    }

    public void processChangeFontPosture() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            String fontpostureString = tochange.getFont().getStyle().toString();
            FontWeight fontweight;
            FontPosture fontposture;
            if(fontpostureString.contains("Bold")){
                fontweight = FontWeight.BOLD;
            }
            else{
                fontweight = FontWeight.NORMAL;
            }
            if(fontpostureString.contains("Italic")){
                fontposture = FontPosture.REGULAR;
            }
            else{
                fontposture = FontPosture.ITALIC;
            }
            Font loadedFont = Font.font(tochange.getFont().getFamily(),fontweight , fontposture, tochange.getFont().getSize());

            ChangeFont_Transaction transaction = new ChangeFont_Transaction(tochange, loadedFont);
            app.processTransaction(transaction);
        }
    }

    public void processChangeFontSizeBytwo(int i) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            String fontpostureString = tochange.getFont().getStyle();
            FontWeight fontweight;
            FontPosture fontposture;
            if(fontpostureString.contains("Bold")){
                fontweight = FontWeight.BOLD;
            }
            else{
                fontweight = FontWeight.NORMAL;
            }
            if(fontpostureString.contains("Italic")){
                fontposture = FontPosture.ITALIC;
            }
            else{
                fontposture = FontPosture.REGULAR;
            }
            Font loadedFont = Font.font(tochange.getFont().getFamily(), fontweight, fontposture, tochange.getFont().getSize()+i);

            ChangeFont_Transaction transaction = new ChangeFont_Transaction(tochange, loadedFont);
            app.processTransaction(transaction);
        }
    }

    public void processChangeTextColor(Color selectColor) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape instanceof DraggableText){
            DraggableText tochange = ((DraggableText)data.selectedDraggableShape);
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            ChangeTextColor_Transaction transaction = new ChangeTextColor_Transaction(tochange, selectColor);
            app.processTransaction(transaction);
        } 
    }

    public void processDuringChangeBorderThickness() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null && !(data.selectedDraggableShape instanceof DraggableImage)){
            ((Shape)data.selectedDraggableShape).setStrokeWidth(((goLogoloWorkspace)(app.getWorkspaceComponent())).borderThicknessSlider.getValue());
        }
    }

    public void transacChangeBorderThickness(double startValueOfborderThicknessSlider, double value) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null && !(data.selectedDraggableShape instanceof DraggableImage)){
            BorderThickness_Transaction transaction = new BorderThickness_Transaction(data.selectedDraggableShape,startValueOfborderThicknessSlider,value);
            app.processTransaction(transaction);
        }
    }

    public void processChangeShapeStrokeColor(Color value) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null && !(data.selectedDraggableShape instanceof DraggableImage)){
            //Font loadedFont = Font.font(Double.parseDouble(fontsize));
            ChangeShapeStrokeColor_Transaction transaction = new ChangeShapeStrokeColor_Transaction(data.selectedDraggableShape,(Color)((Shape)(data.selectedDraggableShape)).getStroke(), value);
            app.processTransaction(transaction);
        }     
    }
    public void processDuringChangeBorderRadius() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null && (data.selectedDraggableShape instanceof DraggableRectangle)){
            //"-fx-border-radius: 30;"
            //System.out.println("123");
            ((DraggableRectangle)data.selectedDraggableShape).setArcHeight(((goLogoloWorkspace)(app.getWorkspaceComponent())).borderRadiusSlider.getValue());
            ((DraggableRectangle)data.selectedDraggableShape).setArcWidth(((goLogoloWorkspace)(app.getWorkspaceComponent())).borderRadiusSlider.getValue());
            //((Shape)data.selectedDraggableShape).setStrokeWidth(((goLogoloWorkspace)(app.getWorkspaceComponent())).borderThicknessSlider.getValue());
        }
    }

    public void transacChangeBorderRadius(double startValueOfborderRadiusSlider, double value) {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null && (data.selectedDraggableShape instanceof DraggableRectangle)){
            BorderRadius_Transaction transaction = new BorderRadius_Transaction(data.selectedDraggableShape,startValueOfborderRadiusSlider,value);
            app.processTransaction(transaction);
        }
    }

    public void processRename(Node x) {

        String newname =AppDialogsFacade.showTextInputDialog(app.getGUIModule().getWindow(), APP_TEXTINPUT_TITLE, APP_TEXTINPUT_CONTENT);
        //((Draggable)x).setName(newname);
        if(newname!=null || newname.equals("")){
            goLogoloData data = (goLogoloData)app.getDataComponent();
            RenameItem_Transaction transaction = new RenameItem_Transaction(data, (Draggable)x,((Draggable)x).getName(),newname);
            app.processTransaction(transaction);     
        }

    }

    public void processMoveUp(Node x) {
        ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        int sel = Nodes.indexOf(x);
        goLogoloData data = (goLogoloData)app.getDataComponent();
        ChangeItemOrder_Transaction transaction = new ChangeItemOrder_Transaction(data, (Draggable)x,Nodes,sel,1);
        app.processTransaction(transaction); 
    }

    public void processMoveDown(Node x) {
        ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
        int sel = Nodes.indexOf(x);
        goLogoloData data = (goLogoloData)app.getDataComponent();
        ChangeItemOrder_Transaction transaction = new ChangeItemOrder_Transaction(data, (Draggable)x,Nodes,sel,2);
        app.processTransaction(transaction); 
    }

    public void processChangeColorGradient(Object value,String changeType) {
        goLogoloWorkspace workspace = ((goLogoloWorkspace)(app.getWorkspaceComponent()));
        if(workspace.startValueOfFocusAngleSlider==0
            ||workspace.startValueOfFocusDistanceSlider==0
            ||workspace.startValueOfCenterXSlider==0
            ||workspace.startValueOfCenterYSlider==0
            ||workspace.startValueOfRadiusSlider==0){
            workspace.byuser=false;
            workspace.updatecolorgradient();
            workspace.byuser=true;
        }
        
        goLogoloData data = (goLogoloData)app.getDataComponent();
        Shape shape = (Shape)data.selectedDraggableShape;
        if(shape!=null && !(shape instanceof DraggableText) &&!((Node)shape instanceof DraggableImage)){
            RadialGradient ori = (RadialGradient)shape.getFill();
            double focusAngle = ((RadialGradient)(shape.getFill())).getFocusAngle();
            double focusDistance = ((RadialGradient)shape.getFill()).getFocusDistance();
            double centerX = ((RadialGradient)shape.getFill()).getCenterX();
            double centerY = ((RadialGradient)shape.getFill()).getCenterY();
            double radius = ((RadialGradient)shape.getFill()).getRadius();
            CycleMethod cycleMethod = ((RadialGradient)shape.getFill()).getCycleMethod();
            Color Stop0Color = ((RadialGradient)shape.getFill()).getStops().get(0).getColor();
            Color Stop1Color = ((RadialGradient)shape.getFill()).getStops().get(1).getColor();;
            
            
            if(changeType.equals("ChangingFocusAngle")){
                focusAngle = (double)value;
            }         
            else if(changeType.equals("TransacFocusAngle")){
                focusAngle = (double)value;
            }
            else if(changeType.equals("ChangingFocusDistance")){
                focusDistance = (double)value;
            }    
            else if(changeType.equals("TransacFocusDistance")){
                focusDistance = (double)value;
            }    
            else if(changeType.equals("ChangingCenterX")){
                centerX = (double)value;
            }    
            else if(changeType.equals("TransacCenterX")){
                centerX = (double)value;
            }    
            else if(changeType.equals("ChangingCenterY")){
                centerY = (double)value;
            }    
            else if(changeType.equals("TransacCenterY")){
                centerY = (double)value;
            }    
            else if(changeType.equals("ChangingRadius")){
                radius = (double)value;
            }    
            else if(changeType.equals("TransacRadius")){
                radius = (double)value;
            }   
            else if(changeType.equals("CycleMethod")){
                String combostring = (String)value;
                if(combostring.equals("NO_CYCLE")){cycleMethod=CycleMethod.NO_CYCLE;}
                if(combostring.equals("REFLECT")){cycleMethod=CycleMethod.REFLECT;}
                if(combostring.equals("REPEAT")){cycleMethod=CycleMethod.REPEAT;}
            }   
            else if(changeType.equals("Stop0 Method")){
                Stop0Color = (Color)value;
            }   
            else if(changeType.equals("Stop1 Method")){
                Stop1Color = (Color)value;
            }
            
            if(!changeType.contains("Changing")){
                
                RadialGradient oldRadialGradient = new RadialGradient(workspace.startValueOfFocusAngleSlider,workspace.startValueOfFocusDistanceSlider,workspace.startValueOfCenterXSlider,workspace.startValueOfCenterYSlider,workspace.startValueOfRadiusSlider,true,ori.getCycleMethod(),new Stop[]{new Stop(0,  ori.getStops().get(0).getColor()), new Stop(1.0,  ori.getStops().get(1).getColor())});
                RadialGradient newRadialGradient = new RadialGradient(focusAngle,focusDistance,centerX,centerY,radius,true,cycleMethod,new Stop[]{new Stop(0,  Stop0Color), new Stop(1.0,  Stop1Color)});
                ChangeColorGradient_Transaction transaction = new ChangeColorGradient_Transaction(shape,oldRadialGradient,newRadialGradient);
                app.processTransaction(transaction);
            }
            else{
                
               
                RadialGradient newRadialGradient = new RadialGradient(focusAngle,focusDistance,centerX,centerY,radius,true,cycleMethod,new Stop[]{new Stop(0,  Stop0Color), new Stop(1.0,  Stop1Color)});
                shape.setFill(newRadialGradient);
            }
            
            
            
        }

    }



}
