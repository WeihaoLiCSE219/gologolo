package glgl.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import glgl.data.Draggable;
import glgl.data.DraggableCircle;
import glgl.data.DraggableRectangle;
import glgl.data.DraggableText;
import glgl.data.DraggableTriangle;
import javafx.scene.control.TextField;
import glgl.goLogoloApp;
import static glgl.goLogoloPropertyType.*;
import glgl.data.goLogoloData;
import glgl.workspace.goLogoloWorkspace;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Shape;


/**
 *
 * @author McKillaGorilla
 */
public class goLogoloSelectionFoolproofDesign implements FoolproofDesign {
    goLogoloApp app;
    
    public goLogoloSelectionFoolproofDesign(goLogoloApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        AppGUIModule gui = app.getGUIModule();
        goLogoloWorkspace workspace = (goLogoloWorkspace)app.getWorkspaceComponent();
        // CHECK AND SEE IF A TABLE ITEM IS SELECTED
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if(data.selectedDraggableShape!=null){
            Draggable node = data.selectedDraggableShape;
            ObservableList<Node> Nodes = ((goLogoloWorkspace)(app.getWorkspaceComponent())).workspacePane.getChildren();
            if(Nodes.indexOf(node)==0){workspace.moveUpButton.setDisable(true);}
            else{workspace.moveUpButton.setDisable(false);}
            
            if(Nodes.indexOf(node)==Nodes.size()-1){workspace.moveDownButton.setDisable(true);}
            else{workspace.moveDownButton.setDisable(false);}
            
            if(node instanceof DraggableRectangle || node instanceof DraggableCircle || node instanceof DraggableTriangle){
                RadialGradient ori = (RadialGradient)(((Shape)node).getFill());
                workspace.byuser=false;
                workspace.FocusAngleSlider.setValue(ori.getFocusAngle());
                workspace.FocusDistanceSlider.setValue(ori.getFocusDistance());
                workspace.CenterXSlider.setValue(ori.getCenterX());
                workspace.CenterYSlider.setValue(ori.getCenterY());
                workspace.RadiusSlider.setValue(ori.getRadius());
                workspace.CycleMethod.setValue(ori.getCycleMethod().toString());
                workspace.Stop0Colorpicker.setValue(ori.getStops().get(0).getColor());
                workspace.Stop1Colorpicker.setValue(ori.getStops().get(1).getColor());
                workspace.byuser=true;
            }
            if(node instanceof DraggableRectangle){
                workspace.byuser=false;
                workspace.borderThicknessSlider.setValue(((DraggableRectangle) node).getStrokeWidth());
                workspace.borderRadiusSlider.setValue(((DraggableRectangle) node).getArcHeight());
                workspace.borderborderColorpicker.setValue((Color)((DraggableRectangle) node).getStroke());
                workspace.byuser=true;
            }
            else if(node instanceof DraggableText){
                workspace.byuser=false;
                workspace.fontcolor.setValue((Color)((DraggableText) node).getFill());
                workspace.fontfamily.setValue(((DraggableText) node).getFont().getFamily());
                workspace.fontsize.setValue(""+(int)((DraggableText) node).getFont().getSize());
                workspace.byuser=true;
            }
            else if(node instanceof DraggableCircle){
                workspace.byuser=false;
                workspace.borderThicknessSlider.setValue(((DraggableCircle) node).getStrokeWidth());
                workspace.borderborderColorpicker.setValue((Color)((DraggableCircle) node).getStroke());
                workspace.byuser=true;
            }
            else if(node instanceof DraggableTriangle){
                workspace.byuser=false;
                workspace.borderThicknessSlider.setValue(((DraggableTriangle) node).getStrokeWidth());
                workspace.borderborderColorpicker.setValue((Color)((DraggableTriangle) node).getStroke());
                workspace.byuser=true;
            }
            
        }
        
//        boolean itemIsSelected = data.isItemSelected();
//        boolean itemsAreSelected = data.areItemsSelected();
//        gui.getGUINode(TDLM_EDIT_ITEM_BUTTON).setDisable(!itemIsSelected);
//        gui.getGUINode(TDLM_REMOVE_ITEM_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
//        if (itemIsSelected) {
//            NodePrototype selectedItem = data.getSelectedItem();
//            int index = data.getItemIndex(selectedItem);
//            gui.getGUINode(TDLM_MOVE_UP_BUTTON).setDisable(index == 0);
//            int numItems = data.getNumItems();
//            System.out.println("numItems: " + numItems);
//            System.out.println("index: " + index);
//            System.out.println("index == (data.getNumItems() - 1): " + (index == (data.getNumItems()-1)));
//            gui.getGUINode(TDLM_MOVE_DOWN_BUTTON).setDisable(index == (data.getNumItems()-1));
//        }
//        else {
//            gui.getGUINode(TDLM_MOVE_UP_BUTTON).setDisable(!itemIsSelected);
//            gui.getGUINode(TDLM_MOVE_DOWN_BUTTON).setDisable(!itemIsSelected);            
//        }
//        ((TextField)gui.getGUINode(TDLM_NAME_TEXT_FIELD)).setEditable(true);
//        ((TextField)gui.getGUINode(TDLM_OWNER_TEXT_FIELD)).setEditable(true);
    }
}