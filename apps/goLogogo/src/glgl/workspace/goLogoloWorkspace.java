package glgl.workspace;

import static djf.AppPropertyType.LANGUAGE_OPTIONS;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import static djf.modules.AppGUIModule.NOT_FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import glgl.goLogoloApp;
import static glgl.workspace.style.TDLStyle.*;
import static glgl.goLogoloPropertyType.*;
import glgl.workspace.controllers.ItemsController;
import glgl.workspace.controllers.ItemsTableController;
import static glgl.workspace.style.TDLStyle.CLASS_TDLM_BOX;
import glgl.data.goLogoloData;

import glgl.workspace.foolproof.goLogoloSelectionFoolproofDesign;
import glgl.transactions.SortItems_Transaction;
import glgl.data.DraggableRectangle;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

/**
 *
 * @author McKillaGorilla
 */
public class goLogoloWorkspace extends AppWorkspaceComponent {
    
    public boolean byuser=true;
    
    public Pane workspacePane;
    public HBox MiddlePane;

    public Slider borderThicknessSlider;
    public Slider borderRadiusSlider;
    
    double borderThicknessSliderReleaseValue;
    double borderRadiusSliderReleaseValue;
    
    double startValueOfborderThicknessSlider;
    double startValueOfborderRadiusSlider;
    

    public ColorPicker borderborderColorpicker;
           
    
    public ColorPicker fontcolor;
    public ComboBox<String> fontfamily;
    public ComboBox<String> fontsize;
    
    public Button moveUpButton;
    public Button moveDownButton;
    
    //Color Gradient
    public Slider FocusAngleSlider;
    public Slider FocusDistanceSlider;
    public Slider CenterXSlider;
    public Slider CenterYSlider;
    public Slider RadiusSlider;
    public ComboBox<String> CycleMethod;
    public ColorPicker Stop0Colorpicker;
    public ColorPicker Stop1Colorpicker;
    
    double FocusAngleSliderReleaseValue;
    double FocusDistanceSliderReleaseValue;
    double CenterXSliderReleaseValue;
    double CenterYSliderReleaseValue;
    double RadiusSliderReleaseValue;
    
    
    public double startValueOfFocusAngleSlider;
    public double startValueOfFocusDistanceSlider;
    public double startValueOfCenterXSlider;
    public double startValueOfCenterYSlider;
    public double startValueOfRadiusSlider;
    
    public goLogoloWorkspace(goLogoloApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();
        
        // 
        initFoolproofDesign();
    }
        
    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder tdlBuilder = app.getGUIModule().getNodesBuilder();
        
        ItemsController itemsController = new ItemsController((goLogoloApp)app);
        
//        app.getGUIModule().getLargerButton().setOnAction(e->{
//            itemsController.processLargerItem();
//        });
        
	// THIS HOLDS ALL THE CONTROLS IN THE WORKSPACE
	
        Pane goLogoloPane = tdlBuilder.buildHBox(GLGL_PANE,null,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        
        VBox LeftPane = tdlBuilder.buildVBox(GLGL_TABLEVIEWPANE,goLogoloPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);

        TableView<Node> NodesTable  = tdlBuilder.buildTableView(GLGL_TABLEVIEW,       LeftPane,          null,   CLASS_TDLM_TABLE, HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  true);
        TableColumn orderColumn      = tdlBuilder.buildTableColumn(  GLGL_ORDER_COLUMN,    NodesTable,         CLASS_TDLM_COLUMN);
        TableColumn nameColumn       = tdlBuilder.buildTableColumn(  GLGL_NAME_COLUMN, NodesTable,         CLASS_TDLM_COLUMN);
        TableColumn typeColumn       = tdlBuilder.buildTableColumn(  GLGL_TYPE_COLUMN,   NodesTable,         CLASS_TDLM_COLUMN);
        
        orderColumn.setCellValueFactory(     new PropertyValueFactory<String,    String>("order"));
        nameColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("name"));
        typeColumn.setCellValueFactory(    new PropertyValueFactory<String, String>("type"));
        
        HBox moveItemPane = tdlBuilder.buildHBox(GLGL_SCROLLTABLEPANE,LeftPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        moveUpButton        = tdlBuilder.buildIconButton(GLGL_MOVEUPBUTTON,      moveItemPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        moveDownButton      = tdlBuilder.buildIconButton(GLGL_MOVEDOWNBUTTON,      moveItemPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button writeorderButton    = tdlBuilder.buildIconButton(GLGL_SCROLLWRITEBUTTON,      moveItemPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);

        MiddlePane = tdlBuilder.buildHBox(GLGL_MIDDLEPANE,goLogoloPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);

        workspacePane=new Pane();
        workspacePane.getStyleClass().add(CLASS_GLGLWORKSPACEPANE);
        //workspacePane.setClip(new Rectangle(workspacePane.getHeight(),workspacePane.getWidth()));
        MiddlePane.getChildren().add(workspacePane);
        //workspacePane.setMaxSize(1000,1000);
//        double sizex= workspacePane.getWidth();
//        double sizey= workspacePane.getHeight();
//        workspacePane.setMaxSize(930,1300);
//        double maxHeight = workspacePane.getMaxHeight();
//        double maxWidth = workspacePane.getMaxWidth();
//        Rectangle clip = new Rectangle(1300,930);
//        workspacePane.setClip(clip);

        
        
        
        //workspacePane = tdlBuilder.buildGridPane(GLGL_WORKSPACEPANE, goLogoloPane, null, CLASS_GLGLWORKSPACEPANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        //DraggableRectangle x = new DraggableRectangle();

        //((goLogoloWorkspace)(app.getWorkspaceComponent())).getWorkspace().getChildren().add(x);
        //workspacePane.getChildren().add(x);
        
        VBox rightPane = tdlBuilder.buildVBox(GLGL_TABLEVIEWPANE,goLogoloPane,null,CLASS_GLGL_RIGHTPANE, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        
        
        
        HBox addShapePane = tdlBuilder.buildHBox(GLGL_ADDSHAPEPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        addShapePane.setAlignment(Pos.CENTER); addShapePane.setSpacing(10);
        Button addTextButton        = tdlBuilder.buildIconButton(GLGL_ADDTEXTBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button addImageButton       = tdlBuilder.buildIconButton(GLGL_ADDIMAGEBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button addRectangleButton   = tdlBuilder.buildIconButton(GLGL_ADDRECTANGLEBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button addCircleButton      = tdlBuilder.buildIconButton(GLGL_ADDCIRCLEBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button addTriangleButton    = tdlBuilder.buildIconButton(GLGL_ADDTRIANGLEBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button removeShapeButton    = tdlBuilder.buildIconButton(GLGL_REMOVESHAPEBUTTON,      addShapePane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        
        HBox editTextPane = tdlBuilder.buildHBox(GLGL_TEXTPROPERTYPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        editTextPane.setSpacing(10);editTextPane.setAlignment(Pos.CENTER);
        ArrayList<String> allFontFamily = new ArrayList<>(Font.getFamilies());
        ArrayList<String> allFontSize = new ArrayList<>();
        for(int i = 40;i<120;i++){
            allFontSize.add(""+i);
        }
        fontfamily = tdlBuilder.buildComboBox(GLGL_CHANGEFONTFAMILYCOMBOBOX,allFontFamily , "Arial", editTextPane, null, CLASS_TDLM_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        fontsize   = tdlBuilder.buildComboBox(GLGL_CHANGEFONTSIZECOMBOBOX,allFontSize , null, editTextPane, null, CLASS_TDLM_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        HBox editTextPane2 = tdlBuilder.buildHBox(GLGL_TEXTPROPERTYPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        editTextPane2.setSpacing(10);editTextPane2.setAlignment(Pos.CENTER);
        Button boldButton    = tdlBuilder.buildIconButton(GLGL_BOLDBUTTON,      editTextPane2,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button italicButton    = tdlBuilder.buildIconButton(GLGL_ITALICBUTTON,      editTextPane2,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button fontsmallerButton    = tdlBuilder.buildIconButton(GLGL_SMALLERBUTTON,      editTextPane2,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button fontbiggerButton    = tdlBuilder.buildIconButton(GLGL_BIGGERBUTTON,      editTextPane2,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        fontcolor    = tdlBuilder.buildColorPicker(GLGL_FONTCOLORCOLORPICKER,      editTextPane2,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);

        VBox borderThickness = tdlBuilder.buildVBox(GLGL_BORDERPROPERTYPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        borderThickness.setAlignment(Pos.CENTER);
        Label borderThicknessLabel = tdlBuilder.buildLabel(GLGL_BORDERTHICKNESSLABLE, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        borderThicknessSlider = tdlBuilder.buildSlider(GLGL_BORDERTHICKNESSSLIDER, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 100);
        borderThicknessSlider.setValue(0);
        
        //VBox borderColor = tdlBuilder.buildVBox(GLGL_BORDERCOLORPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        //borderColor.setAlignment(Pos.CENTER);
        Label borderborderColorLabel = tdlBuilder.buildLabel(GLGL_BORDERCOLORLABLE, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        borderborderColorpicker = tdlBuilder.buildColorPicker(GLGL_BORDERTHICKNESSSLIDER, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        //VBox borderRadius = tdlBuilder.buildVBox(GLGL_RADIUSPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        //borderRadius.setAlignment(Pos.CENTER);
        Label borderRadiusLabel = tdlBuilder.buildLabel(GLGL_BORDERRADIUSLABLE, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        borderRadiusSlider = tdlBuilder.buildSlider(GLGL_BORDERRADIUSSLIDER, borderThickness, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 100);
        borderRadiusSlider.setValue(0);
        
        VBox colorGradient = tdlBuilder.buildVBox(GLGL_COLORGRADIENTPANE,rightPane,null,CLASS_TDLM_BOX, HAS_KEY_HANDLER,FOCUS_TRAVERSABLE,ENABLED);
        colorGradient.setAlignment(Pos.CENTER);
        Label ColorGradientLabel = tdlBuilder.buildLabel(GLGL_COLORGRADIENTLABLE, colorGradient, null, CLASS_LABEL1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        Label FocusAngleLabel = tdlBuilder.buildLabel(GLGL_FOCUSANGLELABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        FocusAngleSlider = tdlBuilder.buildSlider(GLGL_FOCUSANGLESLIDER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, -400,400);
        FocusAngleSlider.setValue(0);
        
        Label FocusDistanceLabel = tdlBuilder.buildLabel(GLGL_FOCUSDISTANCELABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        FocusDistanceSlider = tdlBuilder.buildSlider(GLGL_FOCUSDISTANCESLIDER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, -40,40);
        

        FocusDistanceSlider.setValue(0);

        Label CenterXLabel = tdlBuilder.buildLabel(GLGL_CENTERXLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        CenterXSlider = tdlBuilder.buildSlider(GLGL_CENTERXSLIDER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, -10,10);
        CenterXSlider.setValue(0);
        

        Label CenterYLabel = tdlBuilder.buildLabel(GLGL_CENTERYLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        CenterYSlider = tdlBuilder.buildSlider(GLGL_CENTERYSLIDER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, -10,10);
        CenterYSlider.setValue(0);
        
        Label RadiusLabel = tdlBuilder.buildLabel(GLGL_RADIUSLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        RadiusSlider = tdlBuilder.buildSlider(GLGL_RADIUSSLIDER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, -10, 10);
        RadiusSlider.setValue(0);
        
        Label CycleMethodLabel = tdlBuilder.buildLabel(GLGL_CYCLEMETHODLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ArrayList cycles = new ArrayList<String>();
        cycles.add("NO_CYCLE");cycles.add("REFLECT");cycles.add("REPEAT");
        CycleMethod = tdlBuilder.buildComboBox(GLGL_CYCLEMETHODCOMBOBOX,cycles, null, colorGradient, null, CLASS_TDLM_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label Stop0Label = tdlBuilder.buildLabel(GLGL_STOP0COLORLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Stop0Colorpicker = tdlBuilder.buildColorPicker(GLGL_STOP0COLORCOLORPICKER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label Stop1Label = tdlBuilder.buildLabel(GLGL_STOP1COLORLABLE, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Stop1Colorpicker = tdlBuilder.buildColorPicker(GLGL_STOP1COLORCOLORPICKER, colorGradient, null, CLASS_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);




//        Label toDoListLabel         = tdlBuilder.buildLabel(TDLM_TO_DO_LIST_LABEL,   toDoListPane,   null,   CLASS_TDLM_BIG_HEADER, HAS_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);

        // THIS HAS THE DETAILS PANE COMPONENTS
//        HBox nameOwnerPane          = tdlBuilder.buildHBox(TDLM_NAME_OWNER_PANE1,    toDoListPane,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
//        HBox namePane               = tdlBuilder.buildHBox(TDLM_NAME_PANE,          nameOwnerPane,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
//        Label nameLabel             = tdlBuilder.buildLabel(TDLM_NAME_LABEL,         namePane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
//        TextField nameTextField     = tdlBuilder.buildTextField(TDLM_NAME_TEXT_FIELD,    namePane,       null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
//        HBox ownerPane              = tdlBuilder.buildHBox(TDLM_OWNER_PANE,         nameOwnerPane,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
//        Label ownerLabel            = tdlBuilder.buildLabel(TDLM_OWNER_LABEL,        ownerPane,      null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
//        TextField ownerTextField    = tdlBuilder.buildTextField(TDLM_OWNER_TEXT_FIELD,   ownerPane,      null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
//
//        // THIS HAS THE ITEMS PANE COMPONENTS
//        VBox itemsPane              = tdlBuilder.buildVBox(TDLM_ITEMS_PANE,                 toDoListPane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
//        HBox itemButtonsPane        = tdlBuilder.buildHBox(TDLM_ITEM_BUTTONS_PANE,          itemsPane,          null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
//        Button addItemButton        = tdlBuilder.buildIconButton(TDLM_ADD_ITEM_BUTTON,      itemButtonsPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
//        Button removeItemButton     = tdlBuilder.buildIconButton(TDLM_REMOVE_ITEM_BUTTON,   itemButtonsPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
//        Button editItemButton       = tdlBuilder.buildIconButton(TDLM_EDIT_ITEM_BUTTON,     itemButtonsPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
//        Button moveUpItemButton     = tdlBuilder.buildIconButton(TDLM_MOVE_UP_BUTTON,       itemButtonsPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
//        Button moveDownItemButton   = tdlBuilder.buildIconButton(TDLM_MOVE_DOWN_BUTTON,     itemButtonsPane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);

        // AND NOW THE TABLE
//        TableView<goLogoloItemPrototype> itemsTable  = tdlBuilder.buildTableView(TDLM_ITEMS_TABLE_VIEW,       itemsPane,          null,   CLASS_TDLM_TABLE, HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  true);
//        TableColumn categoryColumn      = tdlBuilder.buildTableColumn(  TDLM_CATEGORY_COLUMN,    itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn descriptionColumn   = tdlBuilder.buildTableColumn(  TDLM_DESCRIPTION_COLUMN, itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn startDateColumn     = tdlBuilder.buildTableColumn(  TDLM_START_DATE_COLUMN,  itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn endDateColumn       = tdlBuilder.buildTableColumn(  TDLM_END_DATE_COLUMN,    itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn assignedToColumn    = tdlBuilder.buildTableColumn(  TDLM_ASSIGNED_TO_COLUMN, itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn completedColumn     = tdlBuilder.buildTableColumn(  TDLM_COMPLETED_COLUMN,   itemsTable,         CLASS_TDLM_COLUMN);
//
//        // SPECIFY THE TYPES FOR THE COLUMNS
//        categoryColumn.setCellValueFactory(     new PropertyValueFactory<String,    String>("category"));
//        descriptionColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("description"));
//        startDateColumn.setCellValueFactory(    new PropertyValueFactory<LocalDate, String>("startDate"));
//        endDateColumn.setCellValueFactory(      new PropertyValueFactory<LocalDate, String>("endDate"));
//        assignedToColumn.setCellValueFactory(   new PropertyValueFactory<String,    String>("assignedTo"));
//        completedColumn.setCellValueFactory(    new PropertyValueFactory<Boolean,   String>("completed"));

	// AND PUT EVERYTHING IN THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setCenter(goLogoloPane);

        // AND NOW SETUP ALL THE EVENT HANDLING CONTROLLERS
//        nameTextField.textProperty().addListener(e->{
//            app.getFileModule().markAsEdited(true);
//        });
//        ownerTextField.textProperty().addListener(e->{
//            app.getFileModule().markAsEdited(true);
//        });
        
//        addItemButton.setOnAction(e->{
//            itemsController.processAddItem();
//        });
//        removeItemButton.setOnAction(e->{
//            itemsController.processRemoveItems();
//        });
//        editItemButton.setOnAction(e->{
//            itemsController.processEditItem();
//        });
        removeShapeButton.setOnAction(e->{
            itemsController.processRemoveItem();
        });
        addRectangleButton.setOnAction(e->{
            itemsController.processAddNode("Rect");
        });
        addTriangleButton.setOnAction(e->{
            itemsController.processAddNode("Tri");
        });
        addTextButton.setOnAction(e->{
            itemsController.processAddNode("Text");
        });
        addImageButton.setOnAction(e->{
            itemsController.processAddNode("Image");
        });
        addCircleButton.setOnAction(e->{
            itemsController.processAddNode("Circle");
        });
        
        boldButton.setOnAction(e->{
            itemsController.processChangeFontWeight();
        });
        italicButton.setOnAction(e->{
            itemsController.processChangeFontPosture();
        });        
        fontsmallerButton.setOnAction(e->{
            itemsController.processChangeFontSizeBytwo(-2);
        });        
        fontbiggerButton.setOnAction(e->{
            itemsController.processChangeFontSizeBytwo(2);
        });       
        fontcolor.setOnAction(e->{
            if(byuser){
                itemsController.processChangeTextColor(fontcolor.getValue());
            }
        });       
        fontsize.getSelectionModel().selectedItemProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(byuser){
                    String selc = fontsize.getSelectionModel().getSelectedItem();
                    itemsController.processChangeFontsize(selc);                
                }
            }
        });     
        fontfamily.getSelectionModel().selectedItemProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(byuser){
                    itemsController.processChangeFont(fontfamily.getSelectionModel().getSelectedItem().toString());                   
                }
            }
        });
        
        

        
        borderThicknessSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processDuringChangeBorderThickness();
            }
	});
        borderThicknessSlider.setOnMousePressed(e-> {
            if(borderThicknessSliderReleaseValue==borderThicknessSlider.getValue()){
                startValueOfborderThicknessSlider = borderThicknessSlider.getValue();
            }
	    //itemsController.processDuringChangeBorderThickness();
	});       
        borderThicknessSlider.setOnMouseReleased(e-> {
            if(startValueOfborderThicknessSlider!=borderThicknessSlider.getValue()){
                itemsController.transacChangeBorderThickness(startValueOfborderThicknessSlider,borderThicknessSlider.getValue());
                borderThicknessSliderReleaseValue = borderThicknessSlider.getValue();
            }
        });
        borderborderColorpicker.setOnAction(e->{
            if(byuser){
                itemsController.processChangeShapeStrokeColor(borderborderColorpicker.getValue());
            }
        });
        borderRadiusSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processDuringChangeBorderRadius();
            }
	});
        borderRadiusSlider.setOnMousePressed(e-> {
            
                        
            if(borderRadiusSliderReleaseValue==borderRadiusSlider.getValue()){
                startValueOfborderRadiusSlider = borderRadiusSlider.getValue();
            }
	    //itemsController.processDuringChangeBorderThickness();
	});       
        borderRadiusSlider.setOnMouseReleased(e-> {
            //System.out.println(""+e.isDragDetect());
            if(startValueOfborderRadiusSlider!=borderRadiusSlider.getValue()){
                itemsController.transacChangeBorderRadius(startValueOfborderRadiusSlider,borderRadiusSlider.getValue());
                borderRadiusSliderReleaseValue = borderRadiusSlider.getValue();
            }
        });
        
        
        
        
        
        
        
        
        
        FocusAngleSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processChangeColorGradient(FocusAngleSlider.getValue(),"ChangingFocusAngle");
            }
	});
        FocusAngleSlider.setOnMousePressed(e-> {
            if(FocusAngleSliderReleaseValue==FocusAngleSlider.getValue()){
                startValueOfFocusAngleSlider = FocusAngleSlider.getValue();
            }  
	});       
        FocusAngleSlider.setOnMouseReleased(e-> {
            if(startValueOfFocusAngleSlider!=FocusAngleSlider.getValue()){
                itemsController.processChangeColorGradient(FocusAngleSlider.getValue(),"TransacFocusAngle");
                FocusAngleSliderReleaseValue = FocusAngleSlider.getValue();
            }
        });
        FocusDistanceSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processChangeColorGradient(FocusDistanceSlider.getValue(),"ChangingFocusDistance");
            }
	});
        FocusDistanceSlider.setOnMousePressed(e-> {
            if(FocusDistanceSliderReleaseValue==FocusDistanceSlider.getValue()){
                startValueOfFocusDistanceSlider = FocusDistanceSlider.getValue();
            }
            
	});       
        FocusDistanceSlider.setOnMouseReleased(e-> {
            if(startValueOfFocusDistanceSlider!=FocusDistanceSlider.getValue()){
                itemsController.processChangeColorGradient(FocusDistanceSlider.getValue(),"TransacFocusDistance");
                FocusDistanceSliderReleaseValue = FocusDistanceSlider.getValue();
            }
        });
        CenterXSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processChangeColorGradient(CenterXSlider.getValue(),"ChangingCenterX");
            }
	});
        CenterXSlider.setOnMousePressed(e-> {
            if(CenterXSliderReleaseValue == CenterXSlider.getValue()){
                startValueOfCenterXSlider = CenterXSlider.getValue();
            }
            
	});       
        CenterXSlider.setOnMouseReleased(e-> {
            if(startValueOfCenterXSlider!=CenterXSlider.getValue()){
                itemsController.processChangeColorGradient(CenterXSlider.getValue(),"TransacCenterX");
                CenterXSliderReleaseValue = CenterXSlider.getValue();
            }
        });
        CenterYSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processChangeColorGradient(CenterYSlider.getValue(),"ChangingCenterY");
            }
	});
        CenterYSlider.setOnMousePressed(e-> {
            if(CenterYSliderReleaseValue == CenterYSlider.getValue()){
                startValueOfCenterYSlider = CenterYSlider.getValue();
            }
            
	});       
        CenterYSlider.setOnMouseReleased(e-> {
            if(startValueOfCenterYSlider!=CenterYSlider.getValue()){
                itemsController.processChangeColorGradient(CenterYSlider.getValue(),"TransacCenterY");
                CenterYSliderReleaseValue = CenterYSlider.getValue();
            }
        });
        RadiusSlider.valueProperty().addListener(e-> {
            if(byuser){
                itemsController.processChangeColorGradient(RadiusSlider.getValue(),"ChangingRadius");
            }
	});
        RadiusSlider.setOnMousePressed(e-> {

            if(RadiusSliderReleaseValue == RadiusSlider.getValue()){
                startValueOfRadiusSlider = RadiusSlider.getValue();
            }
            
	});       
        RadiusSlider.setOnMouseReleased(e-> {
            if(startValueOfRadiusSlider!=RadiusSlider.getValue()){
                itemsController.processChangeColorGradient(RadiusSlider.getValue(),"TransacRadius");
                RadiusSliderReleaseValue = RadiusSlider.getValue();
            }
        });
        
        CycleMethod.getSelectionModel().selectedItemProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(byuser){
                    String selc = CycleMethod.getSelectionModel().getSelectedItem();
                    itemsController.processChangeColorGradient(selc,"CycleMethod");                
                }
            }
        }); 
        Stop0Colorpicker.setOnAction(e->{
            if(byuser){
                itemsController.processChangeColorGradient(Stop0Colorpicker.getValue(),"Stop0 Method");
            }
        });
        Stop1Colorpicker.setOnAction(e->{
            if(byuser){
                itemsController.processChangeColorGradient(Stop1Colorpicker.getValue(),"Stop1 Method");
            }
        });
        
        
        
        
        workspacePane.setOnMousePressed(e -> {
            itemsController.processWorkspacePaneMousePress((int) e.getX(), (int) e.getY());
        });
        workspacePane.setOnMouseReleased(e -> {
            itemsController.processWorkspacePaneMouseRelease((int) e.getX(), (int) e.getY());
        });
        workspacePane.setOnMouseDragged(e -> {
            itemsController.processWorkspacePaneMouseDragged((int) e.getX(), (int) e.getY());
        });
//        workspacePane.setOnMouseClicked(e -> {
//            itemsController.processWorkspacePaneMouseClicked((int) e.getX(), (int) e.getY());
//        });
        NodesTable.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() ==2  &&  NodesTable.getSelectionModel().getSelectedItem()!=null) {
                //itemsController.processSelectItem(NodesTable.getSelectionModel().getSelectedItem());
                //itemsController.processEditItem();
            }
        });
        NodesTable.setOnMouseClicked(e -> {
           
            if (e.getClickCount() == 1 &&  NodesTable.getSelectionModel().getSelectedItem()!=null) {
                itemsController.processSelectItem(NodesTable.getSelectionModel().getSelectedItem());
            }
            app.getFoolproofModule().updateAll();
        });
        writeorderButton.setOnAction(e->{
            if(NodesTable.getSelectionModel().getSelectedItem()!=null){
                itemsController.processRename(NodesTable.getSelectionModel().getSelectedItem());
            } 
        });
        moveUpButton.setOnAction(e->{
            if(NodesTable.getSelectionModel().getSelectedItem()!=null){
                itemsController.processMoveUp(NodesTable.getSelectionModel().getSelectedItem());
            } 
        });      
        moveDownButton.setOnAction(e->{
            if(NodesTable.getSelectionModel().getSelectedItem()!=null){
                itemsController.processMoveDown(NodesTable.getSelectionModel().getSelectedItem());
            } 
        });
        
        app.getGUIModule().exportButton.setOnAction(e->{
            itemsController.processExport();
        });
        app.getGUIModule().resetSizeButton.setOnAction(e->{
            itemsController.processResetSize();
        });
        app.getGUIModule().smallerButton.setOnAction(e->{
            itemsController.processsmaller();
        });
        app.getGUIModule().biggerButton.setOnAction(e->{
            itemsController.processbigger();
        });
        app.getGUIModule().largerButton.setOnAction(e->{
            itemsController.processlarger();
        });
        
//        ItemsTableController iTC = new ItemsTableController(app);
//        itemsTable.widthProperty().addListener(e->{
//            iTC.processChangeTableSize();
//        });
//        itemsTable.setOnSort(new EventHandler<SortEvent<TableView<goLogoloItemPrototype>>>(){
//            @Override
//            public void handle(SortEvent<TableView<goLogoloItemPrototype>> event) {
//                goLogoloData data = (goLogoloData)app.getDataComponent();
//                ArrayList<goLogoloItemPrototype> oldListOrder = data.getCurrentItemsOrder();
//                TableView view = event.getSource();
//                ObservableList sortOrder = view.getSortOrder();
//                if ((sortOrder != null) && (sortOrder.size() == 1)) {
//                    TableColumn sortColumn = event.getSource().getSortOrder().get(0);
//                    String columnText = sortColumn.getText();
//                    SortType sortType = sortColumn.getSortType();
//                    System.out.println("Sort by " + columnText);
//                    event.consume();
//                    SortItems_Transaction transaction = new SortItems_Transaction(data, oldListOrder, columnText, sortType);
//                    app.processTransaction(transaction);
//                    app.getFoolproofModule().updateAll();
//                }
//            }            
//        });
    }
    
    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(GLGL_FOOLPROOF_SETTINGS, 
                new goLogoloSelectionFoolproofDesign((goLogoloApp)app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
       // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    public Pane getWorkspacePane() {
        return workspacePane;
    }

    @Override
    public Pane getMiddleWorkSpace() {
        return workspacePane;
    }

    public HBox getMiddlePane() {
        return MiddlePane;
    }
    public void updatecolorgradient(){
        startValueOfFocusAngleSlider = FocusAngleSlider.getValue();
        startValueOfFocusDistanceSlider = FocusDistanceSlider.getValue();
        startValueOfCenterXSlider = CenterXSlider.getValue();
        startValueOfCenterYSlider = CenterYSlider.getValue();
        startValueOfRadiusSlider = RadiusSlider.getValue();
    }
    

    
    
}