package glgl.clipboard;

import djf.components.AppClipboardComponent;
import glgl.data.Draggable;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import glgl.goLogoloApp;
import glgl.data.goLogoloData;
import glgl.transactions.CutItems_Transaction;
import glgl.transactions.PasteItems_Transaction;
import javafx.scene.Node;

/**
 *
 * @author McKillaGorilla
 */
public class goLogoloClipboard implements AppClipboardComponent {
    goLogoloApp app;
    Node clipboardCutItem;
    Node clipboardCopiedItem;
    
    public goLogoloClipboard(goLogoloApp initApp) {
        app = initApp;
        clipboardCutItem = null;
        clipboardCopiedItem = null;
        
    }
    
    @Override
    public void cut() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if (data.isItemSelected()) {
            clipboardCutItem = (Node)data.selectedDraggableShape;
            clipboardCopiedItem = null;
            CutItems_Transaction transaction = new CutItems_Transaction((goLogoloApp)app, clipboardCutItem);
            app.processTransaction(transaction);
        }
    }

    @Override
    public void copy() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        if (data.isItemSelected()) {
            clipboardCopiedItem = ((Draggable)(Node)data.selectedDraggableShape).copyNode();
            clipboardCutItem = null;
            app.getFoolproofModule().updateAll();   
            
        }
    }
    
//    private void copyToCutClipboard(Node itemToCopy) {
//        clipboardCutItem = copyItem(itemToCopy);
//        clipboardCopiedItem = null;        
//        app.getFoolproofModule().updateAll();        
//    }
    
//    private void copyToCopiedClipboard(Node itemToCopy) {
//        clipboardCutItem = null;
//        clipboardCopiedItem = copyItem(itemToCopy);
//        app.getFoolproofModule().updateAll();        
//    }
    
//    private Node copyItem(Node itemsToCopy) {
//        Node tempCopy=((Draggable)itemsToCopy).copyNode();
//                
//        return tempCopy;
//    }

    @Override
    public void paste() {
        goLogoloData data = (goLogoloData)app.getDataComponent();
        
            //int selectedIndex = data.getItemIndex(data.getSelectedItem());  
            //Node pasteItem = clipboardCutItem;
            if ((clipboardCutItem != null)) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((goLogoloApp)app, clipboardCutItem);
                app.processTransaction(transaction);
                
                // NOW WE HAVE TO RE-COPY THE CUT ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                clipboardCutItem = ((Draggable)clipboardCutItem).copyNode();
                clipboardCopiedItem = null; 
                app.getFoolproofModule().updateAll();      
                //copyToCutClipboard(clipboardCutItem);
            }
            else if ((clipboardCopiedItem != null)) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((goLogoloApp)app, clipboardCopiedItem);
                app.processTransaction(transaction);
            
                // NOW WE HAVE TO RE-COPY THE COPIED ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                clipboardCopiedItem = ((Draggable)clipboardCopiedItem).copyNode();
                clipboardCutItem = null; 
                app.getFoolproofModule().updateAll();   
            }
        
    }    


    @Override
    public boolean hasSomethingToCut() {
        return ((goLogoloData)app.getDataComponent()).isItemSelected();
                
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((goLogoloData)app.getDataComponent()).isItemSelected();
             
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItem != null) )
            return true;
        else if ((clipboardCopiedItem != null) )
            return true;
        else
            return false;
    }
}